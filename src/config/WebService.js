import _ from "lodash";
import Util from "../util";
// export const BASE_URL = "https://dev70.onlinetestingserver.com/water-adventure/api/v1/kiosk/";
// export const BASE_URL = "http://dev70.onlinetestingserver.com/water-adventure-new/api/v1/kiosk/";
export const BASE_URL = "https://dev70.onlinetestingserver.com/water-adventure-2-2/api/v1/kiosk/";
export const API_TIMEOUT = 30000;
export const NEW_API_KEY = "1d399038bef14b0497d028fc27999696";

// API USER ROUTES
export const API_LOG = true;

export const ERROR_SOMETHING_WENT_WRONG = {
  message: "Something went wrong, Please try again later",
  error: "Something went wrong, Please try again later"
};
export const ERROR_NETWORK_NOT_AVAILABLE = {
  message: "Please connect to the working Internet",
  error: "Please connect to the working Internet"
};

export const ERROR_TOKEN_EXPIRE = {
  message: "Session Expired, Please login again!",
  error: "Session Expired, Please login again!"
};

export const REQUEST_TYPE = {
  GET: "get",
  POST: "post",
  DELETE: "delete",
  PUT: "put"
};

// API USER ROUTES


export const USER_SIGNOUT = {
  route: "auth/logout",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};

export const USER_SIGNIN = {
  route: "auth/login",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};



export const UPDATE_USER_PROFILE = {
  route: "update-trainer-profile",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const USER_FORGOT_PASSWORD = {
  route: "password-reset",
  access_token_required: false,
  type: REQUEST_TYPE.POST
};

export const USER_UPDATE_PASSWORD = {
  route: "change-password",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}; 
// activities api
export const GET_ALL_ACTIVITIES = {
  route: "activities",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
export const CREATE_ACTIVITY = {
  route: "add-activity", //"create-activity",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const GET_ALL_PACKAGES = {
  route: "packages", //"create-activity",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
export const ADD_ACTIVITY_PACKAGES = {
  route: "add-inventory",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const GET_BOOKING_TIME_SLOTS = {
  route: "get-available-time-slots",
  access_token_required: true,
  type: REQUEST_TYPE.POST
};
export const GET_CLASS_TYPES = {
  route: "get-class-types",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
// card api
export const CREATE_CARD={
  route: "create-card",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}
export const APPLY_VOUCER={
  route: "apply-coupon",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}
export const GET_CART_ITEMS_ROUTE={
  route: "get-cart-items",
  access_token_required: true,
  type: REQUEST_TYPE.GET
}
export const CHECKOUT={
  route: "checkout",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}
export const UPDATE_CART={
  route: "update-cart",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}
export const REMOVE_CART={
  route: "remove-item",
  access_token_required: true,
  type: REQUEST_TYPE.POST
}
// ----appInfo //

export const TERMS_AND_CONDITION = {
  route: "terms-and-conditions",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
export const ABOUT_US = {
  route: "about",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};

export const PRIVACY_POLICY = {
  route: "privacy-policy",
  access_token_required: true,
  type: REQUEST_TYPE.GET
};
export const GET_ALL_NOTIFICATIONS={
  route: "all-notifications",
  access_token_required: true,
  type: REQUEST_TYPE.GET
}

export const callRequest = function(
  url,
  data,
  parameter,
  header = {},
  ApiSauce,
  baseUrl = BASE_URL
) {
  // note, import of "ApiSause" has some errors, thats why I am passing it through parameters

  let _header = header;
  if (url.access_token_required) {
    const _access_token = Util.getCurrentUserAccessToken();
    if (_access_token) {
      _header = {
        ..._header,
        ...{
          Authorization: `Bearer ${_access_token}`
        }
      };
    }
  }

  const _url =
    parameter && !_.isEmpty(parameter)
      ? `${url.route}/${parameter}`
      : url.route;

  if (url.type === REQUEST_TYPE.POST) {
    return ApiSauce.post(_url, data, _header, baseUrl);
  } else if (url.type === REQUEST_TYPE.GET) {
    return ApiSauce.get(_url, data, _header, baseUrl);
  } else if (url.type === REQUEST_TYPE.PUT) {
    return ApiSauce.put(_url, data, _header, baseUrl);
  } else if (url.type === REQUEST_TYPE.DELETE) {
    return ApiSauce.delete(_url, data, _header, baseUrl);
  }
  // return ApiSauce.post(url.route, data, _header);
};
