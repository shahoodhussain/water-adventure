/**
 * Created by waleed on 13/05/2019.
 */

const theme = {
    primary: '#000',
    secondary:'#f4c217',
    background: '#f5f5f5',
    tabBackground:'#fff',
    heading: '#fff',
    headingLight: '#000',
    text: '#575757',
    textLight: '#999',
    error: '#f04',
    shadow: '#333333',
    success: '#00e676',
};

export default theme;