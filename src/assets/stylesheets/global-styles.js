/**
 * Created by waleed on 13/05/2019.
 */

import Theme from "./theme";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {Dimensions} from 'react-native';
let {width, height} = Dimensions.get('window');

export default {
  container: {
    flex: 1,
    backgroundColor: Theme.background
  },
  scrollView: {
    //backgroundColor: 'pink',
    flexDirection: 'column',
    flex:1,
    //alignItems: 'center',
    justifyContent: 'flex-start',
    marginHorizontal: 10,
    marginVertical:10,
  },
  loginTopContainer: {
    flex: 2,
   },
  loginHeadingContainer: {
    flex: 2,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor: "green",
  },
  loginFieldsContainer: {
    flex: 3.5,
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor: "yellow",
  },
  loginForgetContainer: {
    flex: 1,
    alignItems:'center',
    // backgroundColor: "orange",
  },
  forgetHeadingContainer: {
    width: '39%',
    alignItems: 'flex-end',
  },
  loginButtonContainer: {
    flex: 2,
    alignItems:'center',
    justifyContent:'center',
    //backgroundColor: "blue",
  },
  loginBottomContainer: {
    flex: 3,
   // backgroundColor: "white",
  },

  logoContainer: {
    flex: 2,
    padding: 20,
    alignItems: "center"
  },
  topContainer: {
    flexDirection: "row",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.3,
    shadowRadius: 1.75,
    elevation: 10,
    backgroundColor: "#fff",
  },
  headerLeft: {
    flex: 2.5,
    padding: 24,
    backgroundColor: "#fff",
    marginTop: "4%"
  },
  headerCenter: {
    flex: 6,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    marginTop: "4%"
  },
  headerRight: {
    flex: 2.5,
    padding: 18,
    backgroundColor: "#fff",
    marginTop: "4%"
  },
  textFieldContainer: {
    height: "14%",
    justifyContent: "center",
    alignItems: "center"
  },
  activityBox:{
    flex: 1,
    // width: width/3.3,
    borderRadius:8,
    height: hp('30%'),
    paddingBottom:2,
    margin:10,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.25,
  },

  activityImageContainer:{
    flex:3.2,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    borderTopLeftRadius:8,
    borderTopRightRadius:8,
  },
  selectedActivityImageContainer: {
    flex:3.2,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    borderTopLeftRadius:8,
    borderTopRightRadius:8,
    backgroundColor:'#f06d12'
  },
  activityBottomContainer:{
    flex:0.8,
    flexDirection:'row',
    alignItems:'center',
    borderBottomLeftRadius:8,
    borderBottomRightRadius:8,
    justifyContent:'center',
    backgroundColor:'#fff'
  },

  activityBottomContainerSelected:{
    paddingTop: 12,
    paddingBottom: 12,
    flexDirection:'row',
    alignItems:'center',
    borderBottomLeftRadius:8,
    borderBottomRightRadius:8,
    justifyContent:'center',
    backgroundColor:'#f06d12'
  },

  activityText:{
    fontSize:18,
    fontFamily:"Raleway-Medium",
    color:'#000'
  },
  selectedActivityText:{
    fontSize:18,
    fontFamily:"Raleway-Medium",
    color:'#fff'
  },
  activityTextSelected:{
    fontSize:18,
    fontFamily:"Raleway-Medium",
    color:'#fff'
  },
  
  productHeadingContainer: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    height: 60,
    justifyContent: 'center',
  },

  productText: {
    fontSize: 30,
    fontFamily:"Raleway-Medium",
    color: '#3B5998'
  },
  listContainer: {
    flex: 1,
    backgroundColor: 'red',
    padding: 10,
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  detailTop:{
    flex:1,
  },
  detailMiddle:{
    flex:1,
    resizeMode: 'contain',
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
  },
  detailBottom:{
    flex:2,
    alignItems:'center',
    justifyContent:'center',
    marginBottom: 10,
  },
  detailFooter:{
    flex:1,
  },
  buttonContainer: {
    width: "39%",
    height: 60,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  logoutContainer: {
    width: 20,
    height: 20,
    //flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 20
    //backgroundColor:"#000"
  },
  logoView: {
    elevation: 10,
    // height:220,
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
    //borderRadius:8
  },
  
  play: {
    //borderRadius:8,
    flex: 1,
    resizeMode: "contain",
    //  height: 100,
    //  width: 100
  },
  formGroup:{
    flexDirection:'row',
    // borderColor: 'blue',
    // borderWidth: 1,
    marginTop: 10
  },
  inputContainer: {
    flex:1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  formInput: {
    width: "39%",
    height: 66,
    borderColor: "#000",
    backgroundColor: "rgba(255, 255, 255, 0.8)",
    fontSize: 14,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.25,
    borderRadius: 10,
    elevation: 10,
    paddingLeft: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  formInputDetail: {
    width: "96%",
    height: 46,
    margin:10,
    borderColor: "#000",
    backgroundColor: "rgba(255, 255, 255, 1)",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.25,
    borderRadius: 8,
    elevation: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center"
  },

  formInputDetail2: {
    width: "98%",//"96%",
    height: 46,
    margin:10,
    backgroundColor: "rgba(255, 255, 255, 1)",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.25,
    borderRadius: 8,
    elevation: 10,
    paddingLeft: 10,
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center"
  },

  formInputDetail3:{
    width: "96%",
    height: 46,
    margin:10,
    backgroundColor: "rgba(255, 255, 255, 1)",
    fontSize: 14,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.25,
    borderRadius: 8,
    elevation: 10,
    paddingLeft: 10,
    paddingRight: 10,
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center"
  },

  selectStyle:{
    width:wp('90%'),
    borderColor:'#fff',
  },
  selectStyleHalf: {
    width:wp('42%'),
    borderColor:'#fff',
  },
  labelTop:{
    color:'#fff',
    marginLeft: 15,
    marginTop: 10,
    alignSelf: 'flex-start'
  },
  
 passwordInput: {
    height: 30,
    marginTop: "7%",
    fontSize: 14,
    width: "84%",
    borderBottomWidth: 0.3,
    borderColor: "#000"
    // backgroundColor: "#000"
    // marginLeft:'4%'
  },
  passwordInputText: {
    height: 30,
    marginTop: "7%",
    fontSize: 14,
    width: "84%",
  //  borderBottomWidth: 0.3,
    borderColor: "#000",
   // backgroundColor: "#000"
    // marginLeft:'4%'
  },
  input: {
    flex: 1,
    paddingTop: 9,
    paddingRight: 10,
    paddingBottom: 8,
    paddingLeft: 10,
    //backgroundColor: "rgba(255, 255, 255, 0.5)",
    color: "#000",
    fontSize:18,
    justifyContent: "center",
    alignItems: "center"
  },

  input2: {
    // width: wp('44%'),
    flex:1,
    fontSize:14,
    height:40,
    color: "#000",
    //backgroundColor:'red'
  },
  input2DatePicker: {
    flex:1,
    height:40,
  },

  
  
  loginButton: {
    backgroundColor: Theme.primary,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.5,
    shadowRadius: 4.25,
    borderRadius: 20,
    elevation: 15,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp("6%"),
    flex: 0.4,
    marginRight: "5%",
    height: 40,
    flexDirection: "row"
  },


  loginButtonText:{
      fontSize:20,
      position:'absolute',
      fontWeight:"bold",
      color:Theme.heading
  },

  forgetButton: {
    backgroundColor: Theme.secondary,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.5,
    shadowRadius: 4.25,
    borderRadius: 25,
    elevation: 10,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginRight: "10%",
    marginLeft: "10%",
    height: 45
  },
 

  loginText: {
    color: "#000"
  },
  forget: {
    marginTop: "8%",
    flex: 0.4,
    flexDirection: "row"
  },
  forgetTopContainer: {
    flex: 3.5,
    marginTop:10,
   // backgroundColor:"red"
    //flexDirection:'row',
  },

  firstHeading: {
    fontFamily: 'Oswald-Light',
    fontSize: hp('7%'),
    color: Theme.heading,
   /// fontWeight:'bold',
    padding: 5,
   },

   topHeading:{
    fontFamily: 'Oswald-Medium',
    fontSize: hp('3%'),
    color: Theme.heading,
  },
 
  forgetHeading: {
   fontSize: 16,
   color: Theme.heading,
  //  marginLeft:'22%',
    padding: 5
    // backgroundColor:'red'
  },
  iconImage: { height: 20, width: 40, resizeMode: "contain" },
  iconImage2: { height: 15, width: 23, resizeMode: "contain", },

  searchIconImage: { height: 25, width: 40, resizeMode: "contain" },
  notification: {
    // fontFamily:'San Francisco',
    fontSize: 17,
    fontWeight: "bold",
    color: "#000"
  },
  textView: {
    flex: 4,
    padding: 18,
    // alignItems:'center',
    justifyContent: "center"
  },

  // VALUE ADDED COMPONENT STYLING SECTION
  
  sectionContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  packageAndAccessoriesSection: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
  sectionHeading: {
    fontFamily: 'Oswald-Medium',
    fontSize: hp('3%'),
    color: '#0178C6',
    marginLeft: 5,
    marginTop: 20,
  },
  packageContainer:{
    flex: 1,
    borderRadius:8,
    height:hp('31%'),
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    marginTop: 10,
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.25,
    elevation: 3
  },
  quantityContainer: {
    position: 'absolute',
    top: 10,
    right: 10,
    backgroundColor: '#F06E11',
    elevation: 1,
    zIndex: 99,
    width: 30,
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },

  packagesImageContainer:{
    flex: 1.5,
    borderTopLeftRadius:8,
    borderTopRightRadius:8,
    backgroundColor: '#fff',
  },
  selectedPackageImageContainer: {
    flex: 1.5,
    borderTopLeftRadius:8,
    borderTopRightRadius:8,
    backgroundColor: '#F06E11',
  },
  activityContainerSelected:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    borderBottomLeftRadius:8,
    borderBottomRightRadius:8,
    backgroundColor:'#fff',
  },
  selectedActivityContainerSelected: {
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    borderBottomLeftRadius:8,
    borderBottomRightRadius:8,
    backgroundColor:'#F06E11',
  },
  packageText:{
    fontSize:18,
    fontFamily:"Raleway-Medium",
    color:'#000',
  },
  packagePriceText:{
    fontSize:16,
    fontFamily:"Raleway-Medium",
    color:'#f06d12',
    margin: 8
  },
  selectedPackagePriceText:{
    fontSize:16,
    fontFamily:"Raleway-Medium",
    color:'#fff',
    margin: 8
  },
  addPackageContainer:{
    paddingTop: 6,
    paddingBottom: 6,
    paddingRight: 40,
    paddingLeft: 40,
    backgroundColor:'#f06d12',
    borderRadius: 5
  },
  selectedAddPackageContainer:{
    paddingTop: 6,
    paddingBottom: 6,
    paddingRight: 40,
    paddingLeft: 40,
    backgroundColor:'#fff',
    borderRadius: 5
  },
  addPackageButtonText:{
    fontSize:12,
    fontFamily:"Raleway-Medium",
    color:'#fff'
  },
  selectedAddPackageButtonText:{
    fontSize:12,
    fontFamily:"Raleway-Medium",
    color:'#f06d12'
  },
  floatingNextButton:{
    backgroundColor: '#f06d12',
    height: 75,
    width: 75,
    position: 'absolute',
    right: 20,
    bottom: 20,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 7
  },
  fowardArrow:{
    fontSize:26,
    color:'#fff',
  },

  // package invoice activity component stylyes
  packageRow:{
    flexDirection: 'row',
  },
  voucherHeading:{
    fontSize: 20,
    color: '#000',
    fontWeight: 'bold',
    fontFamily: 'Oswald-Medium',
  },
  mainContainer:{
    height: '82.5%',
    flexDirection: 'row',
  },
  invoiceBox:{
    flex: 1.4,
    paddingTop: 20,
    paddingLeft: 10,
    paddingBottom: 10
  },
  invoiceItem:{
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 3,
    backgroundColor: '#fff',
    borderRadius: 8,
    marginLeft: 10,
    marginBottom: 20,
  },
  invoiceItemImageLandscape:{
    width: 80,
    height: 80,
  },
  invoiceItemImagePortrait:{
    width: 60,
    height: 60,
  },
  invoiceItemTitleLandscape:{
    marginLeft: 20,
    marginRight: 20,
    fontSize: 20,
    color: '#000',
    fontWeight: '500'
  },
  invoiceItemTitlePortrait:{
    marginLeft: 10,
    marginRight: 10,
    fontSize: 18,
    color: '#000',
    fontWeight: '400'
  },
  invoiceItemFirstBox:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  invoiceItemSecondBox:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  itemDetailsPortrait:{
    marginLeft:10,
    marginRight:10,
    height: 60,
    justifyContent: 'center'
  },
  itemDetailsLandscape:{
    marginLeft:20,
    marginRight:10,
    height: 80,
    justifyContent: 'center'
  },
  invoiceSlip:{
    flex: 1,
    padding: 20,
  },
  voucherBox:{
    marginTop: 10,
    flexDirection:'row',
    backgroundColor: '#fff',
    elevation: 2
  },
  applyVoucherButton:{
    backgroundColor: '#f06d12',
    height: 50,
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
  },
  voucherInput:{
    fontSize: 14,
    color: '#A2A2A2'
  },
  dropDown:{
    height: 45,
    fontSize: 14,
    color: '#A2A2A2'
  },
  paymentDetails:{
    marginTop:20,
    marginBottom: 20,
    borderRadius: 10,
    backgroundColor: '#74CFF5'
  },
  amountDetails:{
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  amountText:{
    fontFamily: 'Raleway-light',
    fontSize: 20,
    color: '#fff'
  },
  lastRow:{
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f06d12'
  },
  bookNowBox:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bookNowButton:{
    width: "39%",
    height: 60,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  bookNowButtonText:{
    fontSize:22,
    position:'absolute',
    fontWeight:"500",
    color:Theme.heading,
    fontFamily: 'Raleway-Medium'
  },
  paymentBookNowBox: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkoutButton:{
    width: "39%",
    height: 60,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  // order details component styles
  receiptDetails:{
    flex: 1,
    // width: '100%',
    // marginTop:20,
    // marginBottom: 20,
    // paddingLeft: 30,
    // paddingRight: 30,
    // paddingTop: 10,
    borderRadius: 10,
  },
  receiptHeadingBox:{
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  receiptText:{
    fontSize: 20,
    color: '#000'
  },
  receiptHeading:{
    fontSize: 30,
    fontWeight: '500',
    fontFamily:"Raleway-Medium",
  },
  receiptBox:{
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'center',
  },
  receiptRow:{
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  receiptRowTotal:{
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  orderDetailRow:{
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    marginLeft: 10,
    marginBottom: 20,
  },
  orderInfoBox:{
    marginLeft: 15,
    justifyContent:'center'
  },
  orderValueText:{
    fontSize:22,
    fontFamily:'Raleway-Medium'
  },
  orderDetailsBox:{
    flex: 1.3,
    paddingTop: 20,
    backgroundColor: '#F4F4F4'
  },
  innerRow:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
};
