// // @flow
// import React from "react";
// import { connect } from "react-redux";
// import { Stack, Scene, Router, Actions, Tabs } from "react-native-router-flux";

// import styles from "./styles";
// import { Colors, Images } from "../theme";
// import Util from "../util";

// import { TabIcon } from "../components";

// import {
//   About,
//   Chat,
//   FormBuilder,
//   Home,
//   Login,
//   Policy,
//   Settings,
//   Signup,
//   Terms,
//   SelectOptionList,
//   ForgotPassword,
//   Connect,
//   Resources,
//   Media,
//   More,
//   Events,
//   EventDetail,
//   FindPeer,
//   Profile,
//   News,
//   Mentalhealth,
//   EventsCalendar,
//   ReditFeed,
//   SPDetail,
//   Sponsor,
//   ContactUs,
//   Welcome,
//   OrgProfile,
//   ProfileInner,
//   SPHome,
//   Notifications
// } from "../containers_new";
// import General from "../containers_new/Profile/General";
// import Contact from "../containers_new/Profile/Contact";
// import Clothing from "../containers_new/Profile/Clothing";
// import Marital from "../containers_new/Profile/Marital";
// import Spouse from "../containers_new/Profile/Spouse";
// import Childrens from "../containers_new/Profile/Childrens";
// import Addchild from "../containers_new/Profile/Addchild";
// import professional from "../containers_new/Profile/professional";

// function onBackPress() {
//   if (Actions.state.index === 0) {
//     return false;
//   }
//   Actions.pop();
//   return true;
// }

// const navigator = Actions.create(
//   <Stack
//     key="root"
//     titleStyle={styles.title}
//     headerStyle={styles.header}
//     headerTintColor={Colors.navbar.text}
//   >
//     <Scene
//       key="settings"
//       title="Settings"
//       component={Settings}
//       onLeft={() => Actions.pop()}
//       leftButtonImage={Images.back}
//     />
//     <Scene
//       key="about"
//       title="About"
//       component={About}
//       onLeft={() => Actions.pop()}
//       leftButtonImage={Images.back}
//     />
//     <Scene
//       key="policy"
//       component={Policy}
//       title="Privacy Policy"
//       onLeft={() => Actions.pop()}
//       leftButtonImage={Images.back}
//     />
//     <Scene
//       key="terms"
//       component={Terms}
//       title="Terms & Conditions"
//       onLeft={() => Actions.pop()}
//       leftButtonImage={Images.back}
//     />
//     <Scene
//       key="chat"
//       title="Discussion"
//       component={Chat}
//       onLeft={() => Actions.pop()}
//       leftButtonImage={Images.back}
//     />
//     <Scene
//       key="formBuilder"
//       title="Form Builder"
//       component={FormBuilder}
//       onLeft={() => Actions.pop()}
//       leftButtonImage={Images.back}
//     />
//     <Scene hideNavBar key="dashboard">
//       <Tabs
//         key="tabbar"
//         swipeEnabled={false}
//         showLabel={false}
//         activeBackgroundColor={Colors.white}
//         inactiveBackgroundColor={Colors.white}
//         tabBarPosition="bottom"
//         lazy={!Util.isPlatformAndroid()}
//       >
//         <Stack key="home_tab" title="home" icon={TabIcon}>
//           <Scene key="home_tab" component={Home} hideNavBar />
//         </Stack>
//         <Stack key="connect_tab" title="connect" icon={TabIcon}>
//           <Scene key="connect_tab" component={Connect} hideNavBar />
//         </Stack>
//         <Stack key="resources_tab" title="resources" icon={TabIcon}>
//           <Scene key="resources_tab" component={Resources} hideNavBar />
//         </Stack>
//         <Stack key="media_tab" title="media" icon={TabIcon}>
//           <Scene key="media_tab" component={Media} hideNavBar />
//         </Stack>
//         <Stack key="more_tab" title="more" icon={TabIcon}>
//           <Scene key="more_tab" component={More} hideNavBar />
//         </Stack>
//       </Tabs>
//     </Scene>
//     <Scene hideNavBar key="spdashboard">
//       <Tabs
//         key="tabbar"
//         swipeEnabled={false}
//         showLabel={false}
//         activeBackgroundColor={Colors.white}
//         inactiveBackgroundColor={Colors.white}
//         tabBarPosition="bottom"
//         lazy={!Util.isPlatformAndroid()}
//       >
//         <Stack key="home_tab" title="home" icon={TabIcon}>
//           <Scene key="home_tab" component={SPHome} hideNavBar />
//         </Stack>

//         <Stack key="more_tab" title="more" icon={TabIcon}>
//           <Scene key="more_tab" component={More} hideNavBar />
//         </Stack>
//       </Tabs>
//     </Scene>
//     <Scene key="signup" component={Signup} title="Signup" hideNavBar />
//     <Scene key="forgotPassword" component={ForgotPassword} hideNavBar />
//     <Scene key="selectOptionList" component={SelectOptionList} hideNavBar />
//     <Scene key="eventsList" component={Events} hideNavBar />
//     <Scene key="eventDetail" component={EventDetail} hideNavBar />
//     <Scene key="findPeer" component={FindPeer} hideNavBar />
//     <Scene key="profile" component={Profile} hideNavBar />
//     <Scene key="profileInner" component={ProfileInner} hideNavBar />
//     <Scene key="general" component={General} hideNavBar />
//     <Scene key="contact" component={Contact} hideNavBar />
//     <Scene key="clothing" component={Clothing} hideNavBar />
//     <Scene key="marital" component={Marital} hideNavBar />
//     <Scene key="spouse" component={Spouse} hideNavBar />
//     <Scene key="childrens" component={Childrens} hideNavBar />
//     <Scene key="addchild" component={Addchild} hideNavBar />
//     <Scene key="news" component={News} hideNavBar />
//     <Scene key="mentalhealth" component={Mentalhealth} hideNavBar />
//     <Scene key="professional" component={professional} hideNavBar />
//     <Scene key="eventsCalendar" component={EventsCalendar} hideNavBar />
//     <Scene key="raditfeed" component={ReditFeed} hideNavBar />
//     <Scene key="notifications" component={Notifications} hideNavBar />

//     <Scene
//       key="spDetail"
//       component={SPDetail}
//       hideNavBar
//       // initial
//     />
//     <Scene key="sponsor" component={Sponsor} hideNavBar />
//     <Scene key="login" component={Login} hideNavBar initial />
//     <Scene key="contactUs" component={ContactUs} hideNavBar />
//     <Scene key="welcome" component={Welcome} hideNavBar />
//     <Scene key="orgProfile" component={OrgProfile} hideNavBar />
//   </Stack>
// );

// export default () => (
//   <AppNavigator navigator={navigator} backAndroidHandler={onBackPress} />
// );

// const AppNavigator = connect()(Router);
