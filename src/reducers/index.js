import { combineReducers } from "redux";

// import navigator from "./navigator";

import user from "./user";
import appInfo from"./appInfo";
import activities from "./activities";
import notification from "./notification"
import cards from "./cards";

export default combineReducers({
  // route: navigator,
 
  user,
  appInfo,
  activities,
  notification,
  cards
});
