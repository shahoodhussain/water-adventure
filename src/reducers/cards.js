// @flow
import _ from "lodash";
import Immutable from "seamless-immutable";
import { } from "../actions/ActionTypes";
import{cardList} from "../constants"
import {GET_ALL_CARDS,USER_SIGNOUT} from "../actions/ActionTypes";

const initialState = Immutable({
  data: cardList
});

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_CARDS.SUCCESS: {
   
      return Immutable.merge(state, {
        data: action.data,
      });
    }
      case USER_SIGNOUT.SUCCESS:
        return initialState

    default:
      return state;
  }
};
