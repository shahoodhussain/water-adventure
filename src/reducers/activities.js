// @flow
import _ from "lodash";
import Immutable from "seamless-immutable";
import { GET_ALL_ACTIVITIES,GET_CLASS_TYPES,USER_SIGNOUT, CREATE_ACTIVITY, GET_ALL_PACKAGES, ADD_ACTIVITY_PACKAGES, GET_CART_ITEMS, ACTIVITY_BOOKING_DATE, APPLY_VOUCHER_CODE, CHECKOUT, CLEAR_CHECKOUT_DATA, UPDATE_CART, REMOVE_CART} from "../actions/ActionTypes";

const initialState = Immutable({
  data: [],
  classTypes:[],
});

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ACTIVITY.SUCCESS: {
      return Immutable.merge(state, {
        selectedActivity: action.selectedActivity
      });
    }
    case GET_ALL_ACTIVITIES.SUCCESS: {
    //   const { data, payload } = action;
    //   let tempData = [];
    //   if (payload.offset) {
    //     tempData = _.concat(state.data, data);
    //   } else {
    //     tempData = data;
    //   }
      return Immutable.merge(state, {
        data: action.data,
      });
    }
    case GET_ALL_PACKAGES.SUCCESS: {
      return Immutable.merge(state, {
        addedServices: action.packages
      });
    }
    case ACTIVITY_BOOKING_DATE.SUCCESS: {
      return Immutable.merge(state, {
        timeSlots: action.bookingTimeSlots
      });
    }
    case ADD_ACTIVITY_PACKAGES.SUCCESS: {
      return Immutable.merge(state, {
        addedActivityPackages: action.activityPackages
      });
    }
    case GET_CART_ITEMS.SUCCESS: {
      return Immutable.merge(state, {
        allCartItems: action.cartItems
      });
    }
    case APPLY_VOUCHER_CODE.SUCCESS: {
      return Immutable.merge(state, {
        voucherDetails: action.voucherDetails
      });
    }
    case CHECKOUT.SUCCESS: {
      return Immutable.merge(state, {
        checkoutDetails: action.checkoutData
      });
    }
    case UPDATE_CART.SUCCESS: {
      return Immutable.merge(state, {
        updatedCartData: action.updateCard
      });
    }
    case REMOVE_CART.SUCCESS: {
      return Immutable.merge(state, {
        removeCartData: action.remove_cart
      });
    }
    case GET_CLASS_TYPES.SUCCESS: {
      //   const { data, payload } = action;
      //   let tempData = [];
      //   if (payload.offset) {
      //     tempData = _.concat(state.data, data);
      //   } else {
      //     tempData = data;
      //   }
        return Immutable.merge(state, {
          classTypes: action.data
        });
      }
      case CLEAR_CHECKOUT_DATA.SUCCESS: {
        let newState = {
          data: state.data
        }
        return Immutable.merge(newState)
      }
      case USER_SIGNOUT.SUCCESS:
        return initialState

    default:
      return state;
  }
};
