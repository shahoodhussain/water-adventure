// @flow
import _ from "lodash";
import Immutable from "seamless-immutable";
import { GET_ALL_NOTIFICATIONS,USER_SIGNOUT} from "../actions/ActionTypes";

const initialState = Immutable({
  data: [],
 
});

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_NOTIFICATIONS.SUCCESS: {
    //   const { data, payload } = action;
    //   let tempData = [];
    //   if (payload.offset) {
    //     tempData = _.concat(state.data, data);
    //   } else {
    //     tempData = data;
    //   }
      return Immutable.merge(state, {
        data: action.data
      });
    }

 case USER_SIGNOUT.SUCCESS:
 return initialState

    default:
      return state;
  }
};
