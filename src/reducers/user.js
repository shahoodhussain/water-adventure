// @flow
import Immutable from "seamless-immutable";
import _ from "lodash";
import {
  USER_SIGNIN,
  USER_SIGNOUT,
  UPDATE_USER_PROFILE,
  USER_UPDATE_PASSWORD,
  LOGOUT
} from "../actions/ActionTypes";

const initialState = Immutable({
  data: {},
  access_token: {},
  profileSections: []
});

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_SIGNIN.SUCCESS: {
      return Immutable.merge(state, {
        data: action.data,
        access_token: action.access_token
      });
    }
    case USER_UPDATE_PASSWORD.SUCCESS: {

      let newToken = _.cloneDeep(state.access_token);
      newToken = action.token;

      return Immutable.merge(state, {
        access_token: newToken
      });
    }
    case UPDATE_USER_PROFILE.SUCCESS: {
       let tempData = _.cloneDeep(state.data);

       tempData = action.data;
      console.log("check", tempData);

      return Immutable.merge(state, {
        data: tempData
      });
    }
   
    case USER_SIGNOUT.SUCCESS:
      return initialState;
    default:
      return state;
  }
};
