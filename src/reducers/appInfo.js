// @flow
import Immutable from "seamless-immutable";
import { TERMS_AND_CONDITION,PRIVACY_POLICY,ABOUT_US } from "../actions/ActionTypes";

const initialState = Immutable({
  termsData: [],
  privacyData:[],
  aboutData:[]
});

export default (state: Object = initialState, action: Object) => {
  switch (action.type) {
    case TERMS_AND_CONDITION.SUCCESS:
      return Immutable.merge(state, {
        termsData: action.data
      });
      case ABOUT_US.SUCCESS:
        return Immutable.merge(state, {
          aboutData: action.data
        });
        case PRIVACY_POLICY.SUCCESS:
          return Immutable.merge(state, {
            privacyData: action.data
          });
    default:
      return state;
  }
};
