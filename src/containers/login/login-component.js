/**
 * Created by waleed  on 13/05/2019.
 */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ImageBackground, 
  TextInput,
  // TouchableOpacity,
  // TouchableWithoutFeedback,
  ScrollView,
  Platform,
} from "react-native";
import SplashScreen from "react-native-splash-screen";
import { userSigninRequest } from "../../actions/UserActions";
import { ButtonView, Loader } from "../../components";
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect } from "react-redux";
import _ from "lodash";
import Util from "../../util";
import styles from "../../assets/stylesheets/styles.js";
 //import firebase from 'react-native-firebase';


//import type , { Notification, NotificationOpen } from 'react-native-firebase';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserEmail: "admin@admin.com",
      UserPassword: "admin",
      loading: false,
      device_token:""
    };
  }
  

  goNext = page => {
    this.props.navigation.navigate(page);
  };
  
  

  componentDidMount() {
    
    const {userData}=this.props;
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    SplashScreen.hide();
    //Keyboard.dismiss;

    if (!_.isEmpty(userData.data) && !_.isEmpty(userData.data)) {
      this.props.navigation.navigate("Home");
    }
  }

  Login = () => {
    if (_.isEmpty(this.state.UserEmail)) {
      Util.topAlertError("Please enter your email");
    } 
    else if (!Util.isEmailValid(this.state.UserEmail)) {
      Util.topAlertError("Email is invalid ");
    }
    else if( _.isEmpty(this.state.UserPassword)){
      Util.topAlertError("Please enter your password ");
    }
     else if (!Util.isPasswordValid(this.state.UserPassword)) {
      Util.topAlertError("Password in length should be greater than 5 ");
    } else {
      const payload = {
        email: this.state.UserEmail,
        password: this.state.UserPassword,
        device_type: Platform.OS,
        device_token: this.state.device_token
      };
      Util.showLoader(this);
      this.props.userSigninRequest(payload, data => {
        if (data) {
          //console.log(data,"hhhhhhhhhhhhh");
          setTimeout(() => {
            this.goNext("Home");
          }, 300);
        }
        Util.hideLoader(this);
      });
    }
  };
  
  
  render(){
    return(
      <ScrollView contentContainerStyle={{flexGrow: 1}} style={styles.container}  keyboardShouldPersistTaps="handled">
      {/* <KeyboardAwareScrollView
      style={{ backgroundColor: '#fff' }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.container}
      scrollEnabled={true}> */}

      <ImageBackground source={require('../../assets/images/login_background.png')} style={styles.play}>
        <View style={styles.loginTopContainer}></View>
        
        <View style={styles.loginHeadingContainer}>
          <Text style={styles.firstHeading}>LOGIN</Text>
        </View>

        <View style={styles.loginFieldsContainer}>
          <View style={styles.formInput}>
            <Image source={require('../../assets/images/envelope_icon.png')} style={styles.iconImage} />
            <TextInput style={styles.input} selectionColor='#000' returnKeyType="next" onSubmitEditing={()=>this.secondTextInput.focus()} placeholder='Email Address' autoFocus={true} onChangeText={UserEmail => this.setState({UserEmail})}></TextInput>
          </View>

          <View style={{height:22}}></View>

          <View style={styles.formInput}>
            <Image source={require('../../assets/images/password_icon.png')} style={styles.iconImage} />
            <TextInput style={styles.input} selectionColor='#000' secureTextEntry={true} ref={(input)=>this.secondTextInput = input} placeholder='Password' onChangeText={UserPassword => this.setState({UserPassword})}></TextInput>
          </View>
        </View>

        <View style={styles.loginForgetContainer}>
          <View style={styles.forgetHeadingContainer}>
            <Text style={styles.forgetHeading}>Forgot Your Password?</Text>
          </View>
        </View>
      <View style={styles.loginButtonContainer}>
      
        <ButtonView activeOpacity={0.7} onPress={   this.Login  }  style={styles.buttonContainer}>
          <Image source={require('../../assets/images/login_button.png')} style={styles.play} />  
          <Text style={styles.loginButtonText}>LOGIN</Text> 
        </ButtonView>
      </View>
      <View style={styles.loginBottomContainer}></View>  
      </ImageBackground>
      <Loader loading={this.state.loading} />
    {/* </KeyboardAwareScrollView> */}
    </ScrollView>
    
    );
  }
}
const mapStateToProps = ({ user }) => ({
  userData: user
});
const actions = { userSigninRequest };

export default connect(
  mapStateToProps,
  actions
)(Login);
