/**
 * Created by waleed on 13/05/2019.
 */

import React, { Component } from "react";
import { View, Text, Image as RNImage, TouchableOpacity, FlatList } from "react-native";
import styles from "../../assets/stylesheets/styles.js";
import {connect } from "react-redux";
import Util from "../../util";
import _ from "lodash";
import { Image,Loader,EmptyStateText } from "../../components";
import { getAllActivitiesRequest } from "../../actions/ActivitiesAction";
import { userSignOutRequest} from "../../actions/UserActions";

class HomePageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      imageURL: 'https://dev70.onlinetestingserver.com/water-adventure-new/public/uploads/',
      indexChecked: '0',
      activities: []
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle:(
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          <Image
            resizeMode="center"
            source={require('../../assets/images/logo.png')}
            style={{width:110, height:50}}
          />
        </View> 
      )
    }
  }
  
  componentDidMount(){
    this._getActivities();
  }

  _getActivities=()=> {
    Util.showLoader(this);
      this.props.getAllActivitiesRequest((data)=>{
        if(data){
          this.setState({activities: data});
          Util.hideLoader(this);
      }
    });
  }

  _selectActivity = (item)  => {
    this.setState({ indexChecked: item.id })
    //setTimeout(() => {
      this.props.navigation.navigate("ActivityDetail",{"item":item});
    //}, 500);
  }


  renderActivities = (item) => {
    if( item == 'EMPTYCONTAINER') {
      return <View style={{flex: 1}} />
    }
    return (
      <TouchableOpacity onPress={()=> this._selectActivity(item) } style={styles.activityBox}>
        <View style={this.state.indexChecked === item.id?styles.selectedActivityImageContainer:styles.activityImageContainer}>
          <RNImage source={{uri:this.state.imageURL+item.image}} style={{width:'100%',height:'100%',borderRadius:8, resizeMode: "cover"}} />
        </View>
        <View style={this.state.indexChecked === item.id?styles.activityBottomContainerSelected:styles.activityBottomContainer} >
          <Text style={this.state.indexChecked === item.id?styles.activityTextSelected:styles.activityText}>{item.activity_name}</Text>
        </View>
      </TouchableOpacity>
    )
  }
  
  _renderEmptyComponent =  (isLoading)=> {
    if (isLoading) {
      return null;
    }
    return (
     <EmptyStateText/> 
    );
  };

  render() {
    const { loading } = this.state;
    let activities_temp = this.state.activities;
    if( (activities_temp.length % 3) == 1 ) {
      activities_temp.push('EMPTYCONTAINER')
      activities_temp.push('EMPTYCONTAINER')
    } else if ( (activities_temp.length % 3) == 2) {
      activities_temp.push('EMPTYCONTAINER')
    }
    return (
      <View style={styles.container} >
        {/* <View style={styles.productHeadingContainer}>
          <Text style={styles.productText}>OUR PRODUCTS</Text>
        </View> */}
        <FlatList
        data={activities_temp}
        numColumns={3}
        renderItem={({item}) => this.renderActivities(item)}
        keyExtractor={item => item.id}
        ListEmptyComponent={()=>this._renderEmptyComponent(loading)}
        refreshing={loading}
        showsVerticalScrollIndicator={false}
        onRefresh={this._getActivities}
        />
        <Loader loading={this.state.loading} />
      </View>
    );
  }
}



const mapStateToProps = ({ activities  }) => ({
  activities:activities.data,
});

const actions = { getAllActivitiesRequest,userSignOutRequest };

export default connect(
  mapStateToProps,
  actions
)(HomePageComponent);
