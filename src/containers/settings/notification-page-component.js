import React from 'react';
import { View, Text,FlatList,SafeAreaView } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import { getNotificationRequest } from "../../actions/NotificationActions";
import { connect } from "react-redux";
import Util from "../../util";
import {  EmptyStateText } from "../../components";
import {DATE_FORMAT2,TIME_FORMAT1} from "../../constants";


class NotificationPageComponent extends React.Component {
    state={
        loading: false
    }
    componentDidMount() {
      this._getNotifications()
      //  MessageBarManager.registerMessageBar(this.refs.alert);
    }
    _getNotifications=()=>{
        Util.showLoader(this);
        this.props.getNotificationRequest((data)=>{
        if(data){
            Util.hideLoader(this);
        }
    });
    }


    renderItems({item}) {
        let name=JSON.parse(item.message);
        let date=name.start_class_time;
        let FormatDate=Util.getFormattedDateTime(date,DATE_FORMAT2);
        let formatTime=Util.getFormattedDateTime(date,TIME_FORMAT1)
        return(
            <View style={styles.flatview}>
                <View style={styles.textView}>

                    <Text 
                    
                    numberOfLines={2}
                    style={styles.notification2}>{name.subject}</Text>
                    <Text style={styles.time}>{FormatDate} On {formatTime}</Text>

                </View>
            </View>
        )
    };
    _renderEmptyComponent =  (isLoading)=> {
        if (isLoading) {
          return null;
        }
        return (
         <EmptyStateText/> 
        );
      };
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: "#d5e1e8",
                }}
            />
        );
    };
    render() {
        const { loading } = this.state;
        const {data}=this.props;

        return (
            <SafeAreaView style={styles.navigationContainer}>
                <View style={styles.topView}>
                    <Text style={styles.settingHeading}>Notifications</Text>
                </View>

                <View style={styles.notificationView}>



                    <FlatList

                        data={data}
                        renderItem={this.renderItems}
                        keyExtractor={item => item.key}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListEmptyComponent={()=>this._renderEmptyComponent(loading)}
                        refreshing={loading}
                        showsVerticalScrollIndicator={false}
                        onRefresh={this._getNotifications}
                    />



                </View>

            </SafeAreaView>
        );
    }
}


const mapStateToProps = ({ notification}) => ({
    data:notification.data 
});

const actions = { getNotificationRequest };

export default connect(
  mapStateToProps,
  actions
)(NotificationPageComponent);

