/**
 * Created by faraz ali on 13/05/2019.
 */

import React, { Component } from "react";
import { View, Text, FlatList, TouchableOpacity, Alert } from "react-native";
import styles from "../../assets/stylesheets/styles.js";
import ToggleSwitch from "toggle-switch-react-native";
import { userSignOutRequest ,updateUserProfileRequest} from "../../actions/UserActions";
import { connect } from "react-redux";
import Util from "../../util";
import { Loader } from "../../components";

class SettingsPageComponent extends Component {
  constructor(props) {
    super(props);
   const {userData}=this.props;
   console.log(userData,"check")
  this.state = {
    isOn: userData.allow_notification==="true"?true:false,
    loading: false
  };

  }

  goAhead = page => {
    this.props.navigation.navigate(page);
  };
updateProfile=(isOn)=>{
  this.setState({ isOn: !this.state.isOn })
 // isOn===true 1:
 const payload={
  allow_notification:isOn
 }
  this.props.updateUserProfileRequest(payload)
}
  _renderItem = ({ item }) => {
    if (item.key == "Notification") {
      return (
        <TouchableOpacity activeOpacity={0.5} onPress={() =>{} } style={{

          borderBottomWidth: 0.5,
          borderBottomColor:"#CED0CE",
         flexDirection:"row", flex:1,
         justifyContent:"center",
         alignItems:"center"
          
          

        }}>
          <TouchableOpacity style={{marginLeft:20,
           //  backgroundColor:"red", 
          flex:8,
          paddingVertical:20

        }}
          onPress={() => item.onPress()}
          >
           <Text style={{color: "#000",
           fontSize: 18,
           fontWeight: "bold",
           flex: 1
          }}>Notification</Text>
           </TouchableOpacity>
           <View style={{
            // backgroundColor:"green", 
             flex:2}}>
              <ToggleSwitch
                isOn={this.state.isOn}
                onColor="#f4c217"
                offColor="#aeaeae"
               // label="Notifications"
                labelStyle={{
               //   backgroundColor:"green",
                  color: "#000",
                  fontSize: 18,
                  fontWeight: "bold",
                  flex: 1
                }}
                size="small"
                onToggle={isOn =>
                 // console.log(isOn)
                
                this.updateProfile(isOn)

              }
              />
              </View>
          
       
          
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity activeOpacity={0.5} onPress={() => item.onPress()}>
          <View style={styles.flatview}>
            <View style={styles.textView}>
              <Text style={styles.notification}>{item.key}</Text>
            </View>
            <Loader loading={this.state.loading} />
          </View>
        </TouchableOpacity>
      );
    }
  };
  _userSignOutRequest = () => {
    Alert.alert(
      "Logout",
      "Are you sure?",
      [
        {
          text: "Yes",
          onPress: () => {
            Util.showLoader(this);
            this.props.userSignOutRequest(() => {
              Util.hideLoader(this);
              setTimeout(() => {
                this.props.navigation.navigate("Login");
              }, 500);
            });
          }
        },
        { text: "No", style: "cancel" }
      ],

      { cancelable: true }
    );
  };
  render() {
    return (
      <View style={styles.navigationContainer}>
        <View style={styles.settingsTopView}>
          <Text style={styles.settingHeading}>Settings</Text>
        </View>

        <View style={styles.notificationView}>
          <FlatList
            data={[
              {
                key: "Notification",
                route: "Notification",
                onPress: () => {
                  this.props.navigation.navigate("Notification");
                }
              },
              {
                key: "About Us",
                route: "AboutUs",
                onPress: () => {
                  this.props.navigation.navigate("AboutUs");
                }
              },
              {
                key: "Privacy Policy",
                route: "Privacy",
                onPress: () => {
                  this.props.navigation.navigate("Privacy");
                }
              },
              {
                key: "Terms & Conditions",
                route: "Terms",
                onPress: () => {
                  this.props.navigation.navigate("Terms");
                }
              },
              {
                key: "Edit Profile",
                route: "Profile",
                onPress: () => {
                  this.props.navigation.navigate("Profile");
                }
              },
              {
                key: "Logout",
                route: "Login",
                onPress: () => this._userSignOutRequest()
              }
            ]}
            keyExtractor={item => item.key}
            renderItem={this._renderItem}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ user  }) => ({
  userData: user.data

});

const actions = { userSignOutRequest,updateUserProfileRequest };

export default connect(
  mapStateToProps,
  actions
)(SettingsPageComponent);
