import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text,Image,TextInput,TouchableOpacity ,ScrollView,KeyboardAvoidingView} from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import Util from "../../util";
import { updatePasswordRequest } from "../../actions/UserActions";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Loader } from "../../components";


class ChangePasswordPageComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
        oldPassword:"",
        newPassword:"",
        confirmPassword:"",
        loading: false
        };
      }
      _submit=()=>{
          const {oldPassword,newPassword,confirmPassword}=this.state;
        //   var values=password.forEach(element => {
        //    if(_.isEmpty(element.oldPassword) ){

        //    }   
        //   });
          if(_.isEmpty(oldPassword)){
              Util.topAlertError("Old Password is Empty")
          }
          else if(!Util.isPasswordValid(oldPassword)){
            Util.topAlertError("Password length should be greater than 5")

          }
        
          else if(_.isEmpty(newPassword)){
            Util.topAlertError("New Password is Empty")

          }
          else if(!Util.isPasswordValid(newPassword)){
            Util.topAlertError("Password length should be greater than 5")

          }

         
          else if(_.isEmpty(confirmPassword)){
            Util.topAlertError("Confirm Password is Empty")

          }
          else if(!Util.isPasswordValid(confirmPassword)){
            Util.topAlertError("Password length should be greater than 5")

          }
          else if(newPassword!==confirmPassword){
              Util.topAlertError("password doesn't match")
          }
          else{
            Util.showLoader(this);

const payload={
    oldPassword:oldPassword,
    newPassword:newPassword,
    confirmPassword:confirmPassword,

}
            this.props.updatePasswordRequest(payload,()=>{
            this.props.navigation.navigate('Settings')
            Util.showLoader(this);
            Util.topAlert("Password changed successfully")
            });

          }
      }
  _submitOldPassword=()=>{
    this.newpassword.focus();
  }
  _submitNewPassword=()=>{
    this.confirmpassword.focus();
  }

    render(){
      return(
        <KeyboardAwareScrollView 
        enableOnAndroid
        keyboardShouldPersistTaps={"handled"}
       style={{backgroundColor:"#fff" }}
        showsVerticalScrollIndicator={false}>
          <View style={styles.navigationContainer}>

              <View style={styles.profileTopView}>
                  <Text style={styles.settingHeading}>Change Password</Text>
              </View>

              <View style={styles.profileFieldsView}>

                      <View style={styles.passwordInputText}>

                          <TextInput password style={styles.profileInputField} selectionColor='#900816'  placeholder='Old Password' onChangeText={oldPassword => this.setState({oldPassword})} maxLength={32}
                          ref={ref => {
                            this.oldpassword = ref;
                          }}  
                          onSubmitEditing={this._submitOldPassword} 
                          returnKeyType={"next"}
                          secureTextEntry                      
                           ></TextInput>


                      </View>
                      <View style={styles.passwordInputText}>

                          <TextInput password style={styles.profileInputField} selectionColor='#900816'  placeholder='New Password' onChangeText={newPassword => this.setState({newPassword})} maxLength={32}
                          ref={ref => {
                            this.newpassword = ref;
                          }} 
                          onSubmitEditing={this._submitNewPassword}
                          returnKeyType={"next"}                      
                          secureTextEntry
                          ></TextInput>

                      </View>
                      <View style={styles.passwordInputText}>

                          <TextInput password style={{
                            flex: 1,
                            paddingTop: 0,
                            paddingRight: 10,
                            paddingBottom: 5,
                            paddingLeft: 0,
                            borderBottomColor: "#000",
                            borderBottomWidth: 0.5,
                        
                            color: "#424242"
                          }} selectionColor='#900816'  placeholder='Confirm Password' onChangeText={confirmPassword => this.setState({confirmPassword})} maxLength={32}
                          ref={ref => {
                            this.confirmpassword = ref;
                          }} 
                          onSubmitEditing={this._submitConfirmPassword}
                          returnKeyType={"done"}
                          secureTextEntry
                          ></TextInput>

                      </View>
                </View>
                  <View style={ {flex: 1.5,
                    flexDirection: "row",
                  //  backgroundColor:"red",
                    marginTop: "15%"}}>
                      <TouchableOpacity  activeOpacity={0.7} onPress={this._submit} style={styles.profileButton}>
                          <Text style={{fontSize:18,color:'#000'}}>Submit</Text>
                      </TouchableOpacity>
                  </View>
                <View style={styles.lowerContainer}></View>

                </View>
                <Loader loading={this.state.loading} />

          </KeyboardAwareScrollView>

      );
    };
}
const mapStateToProps = () => ({});

const actions = {updatePasswordRequest};

export default connect(
  mapStateToProps,
  actions
)(ChangePasswordPageComponent);