import React, { Component } from "react";
import { View, Text,Image as RNImage,TouchableOpacity,TextInput,ScrollView ,ActivityIndicator,KeyboardAvoidingView} from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import { updateUserProfileRequest } from "../../actions/UserActions";
import { connect } from "react-redux";
import Util from "../../util";
import _ from "lodash";
import { Loader,Image } from "../../components";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/dist/Ionicons';
import ImagePicker from 'react-native-image-picker'; 
import { Images } from "../../theme/index.js";
import { SafeAreaView } from "react-navigation";


class EditProfilePageComponent extends Component {
    constructor(props) {
        super(props);
        const {data}=this.props;
        console.log(data.name,"namecheck");
        this.state = {
          userName: data.name,
          userEmail: data.email,
          mobile:data.mobile_num,
          avatarSource:data.profile_pic===null ? "https://i.imgur.com/aqx35AH.jpg": `http://olxseventyseven.com/forklift/public/assets/uploads/${data.profile_pic}`,
          profilePicture:"",
          loading: false
        };
      }
_submit=()=>{

const {userName,userEmail,mobile}=this.state;
if(_.isEmpty(userName)){
    Util.topAlertError("Name Field is Empty ")
}
else if(_.isEmpty(userEmail) ){
    Util.topAlertError("Email Field is Empty ")
}
else if(!Util.isEmailValid(userEmail)){
    Util.topAlertError("Email is invalid ")
}
else if(_.isEmpty(mobile)){
    Util.topAlertError("Mobile number is Empty")
}
else {
    Util.showLoader(this);
    const formData = new FormData()
    formData.append('name', this.state.userName);
    formData.append('email', this.state.userEmail);
    formData.append('mobile_num', this.state.mobile);
    const payload={
        uri: this.state.avatarSource,
        type: 'image/png',
        name: "profile_picture"
    }
formData.append("picture",payload);
    // const payload={
    //     name:this.state.userName,
    //     email:this.state.userEmail,
    //     mobile_num:this.state.mobile
    //     }
            this.props.updateUserProfileRequest(formData,()=>{
                this.props.navigation.navigate("Settings")
                Util.showLoader(this);
                Util.topAlert("Profile updated successfully")
            });
}

}
_submitName=()=>{
  this.email.focus();
}
_submitEmail=()=>{
    this.mobile.focus();
}
selectPhotoTapped=()=> {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source =   response.uri ;

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        
       
          this.setState({ avatarSource: source});
          this.setState({profilePicture:source});
        console.log("check",source)

      }
    });
  }
    render(){
        const {data}=this.props;
        let imageSource=`http://olxseventyseven.com/forklift/public/assets/uploads/${data.profile_pic}`
        return(
           
            <KeyboardAwareScrollView 
            enableOnAndroid
            keyboardShouldPersistTaps={"handled"}
           style={{backgroundColor:"#fff" }}
            showsVerticalScrollIndicator={false}>
            <SafeAreaView style={styles.navigationContainer}>
            <View style={styles.profileTopView}>
            <Text style={styles.settingHeading}>Edit Profile</Text>
        </View>
        
                <View style={styles.middleView}>
                    <View style={{
                    
                        flexDirection: "row",
                        alignItems: "center",
                        height: 100,width: 100,
                        overflow:"hidden",
                        borderRadius:
                        50,
                    
                    }}>
                        <Image style={styles.homeProfile}
                        source={{uri:this.state.avatarSource}} 
                        resizeMode="cover"
                        />
                        </View>
<TouchableOpacity style={{ overflow:"visible",position:"absolute",alignSelf:"center",marginTop:60,paddingLeft:70}}
            onPress={()=>this.selectPhotoTapped()}
>
                        <Icon style={{ 
                        fontSize:35,
                      //  position:"absolute",
                     //   alignSelf:"baseline"
                    }} 
                    name="ios-camera"

                />
                </TouchableOpacity>
              
                    {/*<View style={styles.imageView2}>*/}
                        {/*<Image style={[styles.play]} source={require('../../assets/images/logo.png')} />*/}
                    {/*</View>*/}
                </View>
               <View style={styles.dateView}>
                   {/* <View style={{flex:0.5, alignItems:'center'}}><Text style={{textAlign:'justify',color:'#aeaeae'}}>Exp Date</Text><Text style={{fontSize:18}}>20 Dec 2020</Text></View>*/}
                    <View style={{flex:1 , alignItems:'center',justifyContent:"center",alignSelf:"center"}}><Text style={{color:'#aeaeae'}}>Trainer Type</Text><Text style={{fontSize:18}}>
                    
                   {data.utype==="fltrainer"?"ForkLift Trainer":"Company Trainer"} 
                    
                    </Text></View>
                    </View>
                <View style={styles.profileFieldsView}>
                

                    <View style={styles.profileInput}>

                        <TextInput 
                        style={styles.profileInputField} 
                        selectionColor='#900816' 
                        placeholder='Name' 
                        onChangeText={userName => this.setState({userName})
                        
                
                    }
                    returnKeyType="next"

                    value={this.state.userName}
                    onSubmitEditing={this._submitName}
                    ref={ref => {
                        this.name = ref;
                      }}
                    ></TextInput>


                    </View>
                    <View style={styles.profileInput}>

                        <TextInput password 
                        returnKeyType="next"

                        style={styles.profileInputField} selectionColor='#900816'  placeholder='Email Address' onChangeText={userEmail => this.setState({userEmail})}
                        value={this.state.userEmail}
                        onSubmitEditing={this._submitEmail}
                        ref={ref => {
                            this.email = ref;
                          }}
                        ></TextInput>

                    </View>
                    <View style={styles.profileInput}>

                        <TextInput style={styles.profileInputField} selectionColor='#900816' placeholder='Mobile Number' onChangeText={mobile => this.setState({mobile})}
                        value={this.state.mobile}
                        returnKeyType="done"
                        ref={ref => {
                            this.mobile = ref;
                          }}
                          keyboardType="numeric"
                        maxLength={15}
                        ></TextInput>


                    </View>
                    <TouchableOpacity  activeOpacity={0.7} onPress={()=>this.props.navigation.navigate('ChangePassword')} style={{flex:0.4,flexDirection:'row',marginLeft:'50%',padding:16}}><Text style={{color:'#000'}}>Change Password</Text></TouchableOpacity>
                   
                </View>
                <View style={styles.profileLowerView}>
                    <TouchableOpacity  activeOpacity={0.7} onPress={
                      this._submit
                    //    ()=>this.props.navigation.navigate('Settings')
                    }
                     style={styles.profileButton}>
                        <Text style={{fontSize:18,color:'#000'}}>Save</Text>
                    </TouchableOpacity>
                </View>
                
                <Loader loading={this.state.loading} />

            </SafeAreaView>
        </KeyboardAwareScrollView>

           
        );
    }
}


const mapStateToProps = ({ user }) => ({ data: user.data });

const actions = { updateUserProfileRequest };

export default connect(
  mapStateToProps,
  actions
)(EditProfilePageComponent);

