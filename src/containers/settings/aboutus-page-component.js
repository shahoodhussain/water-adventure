import React, { Component } from "react";
import { View, Text,Image ,ScrollView,SafeAreaView,Platform} from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import { aboutRequest } from "../../actions/AppInfoActions";
import { connect } from "react-redux";
import HTML from 'react-native-render-html';
import { Loader } from "../../components";
import Util from "../../util";


class AboutUsPageComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
         
          loading: false
        };
      }
    componentDidMount(){
        Util.showLoader(this);
        this.props.aboutRequest(()=>{
        Util.hideLoader(this);
        });
    }
    
    render() {
        const {data}=this.props;
        return (
        
            <View style={styles.navigationContainer}>
            <SafeAreaView style={Platform.OS==="ios"?
                { marginTop:60, marginHorizontal:20}
            :
               
            { marginTop:20, marginHorizontal:20}
              
            }>
            <HTML html={data}  />
        </SafeAreaView>
        <Loader loading={this.state.loading} />
        </View>
        );
    };
}

const mapStateToProps = ({ appInfo }) => ({
    data: appInfo.aboutData
  });
  const actions = { aboutRequest };
  
  export default connect(
    mapStateToProps,
    actions
  )(AboutUsPageComponent);




