// @flow
import _ from "lodash";
import { connect } from "react-redux";
import React, { Component } from "react";
import { View, Image , Alert } from "react-native";
import PropTypes from "prop-types";
import { Actions } from "react-native-router-flux";
import DataHandler from "../../services/DataHandler";

import { Images, Colors } from "../../theme";
import styles from "./styles";
import Util from "../../util";

class Welcome extends Component {
  static propTypes = {
    userData: PropTypes.object.isRequired
  };

  componentDidMount() {
    Alert.alert("hello");
    const { userData } = this.props;
    //console.log(this.props);
    this.props.navigation.navigate("Home");
    // setTimeout(() => {
    //   if (!_.isEmpty(userData.data) && !_.isEmpty(userData.access_token)) {
    //     this.props.navigation.navigate("Login");
    //   } else {
    //     this.props.navigation.navigate("Login");
    //   }
    // }, 0);
  }

  render() {
    return null;
  }
}

const mapStateToProps = ({ user }) => ({
  userData: user
});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(Welcome);
