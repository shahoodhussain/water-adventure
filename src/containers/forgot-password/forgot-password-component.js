/**
 * Created by faraz ali on 13/05/2019.
 */

import React, { Component } from "react";
import { View, Text, Image, TextInput, TouchableOpacity } from "react-native";
import styles from "../../assets/stylesheets/styles.js";
import _ from "lodash";
import Util from "../../util";
import { forgotPasswordRequest } from "../../actions/UserActions";
import { connect } from "react-redux";
import { Loader } from "../../components";

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserEmail: "",
      loading: false
    };
  }
  _submit = () => {
    if (_.isEmpty(this.state.UserEmail)) {
      Util.topAlertError("Please input Email");
    } else if (!Util.isEmailValid(this.state.UserEmail)) {
      Util.topAlertError("Please input ValidEmail");
    } else {
      const payload = {
        email: this.state.UserEmail
      };
      Util.showLoader(this);
      this.props.forgotPasswordRequest(payload, () => {
        this.props.navigation.navigate("Login");
        Util.showLoader(this);
      });
    }
  };
  render() {
    return (
      <View style={styles.container}>
        
        <Loader loading={this.state.loading} />

      </View>
    );
  }
}
const mapStateToProps = () => ({});

const actions = {
  forgotPasswordRequest
};

export default connect(
  mapStateToProps,
  actions
)(ForgetPassword);
