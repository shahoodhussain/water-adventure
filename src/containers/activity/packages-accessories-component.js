import React from 'react';
import { View, StyleSheet, Text, Image as RNImage, TouchableOpacity, FlatList } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import _ from "lodash";
import { Image,Loader,EmptyStateText, ButtonView } from "../../components";
import Util from "../../util";
import { connect } from "react-redux";
import { getAllPackgesRequest, createActivityPackageRequest } from "../../actions/ActivitiesAction";
import Icon from "react-native-vector-icons/FontAwesome5";

class PackagesAccessoriesComponent extends React.Component {

    state = {
        loading: false,
        language: '',
        imageURL : 'http://dev70.onlinetestingserver.com/water-adventure-new/public/uploads/',
        indexChecked: '0',
        extras: {
            "activity_id": null,
            "packages": [],
            "products": []
        }
    };
    productsIndex = null;
    packagesIndex = null;

    static navigationOptions = ({ navigation }) =>  {
        return{
            headerTitle:( 
                <View style={{flex:1, flexDirection:'row', justifyContent:'center', marginRight: 55}}>
                    <Image
                        resizeMode="center"
                        source={require('../../assets/images/logo.png')}
                        style={{width:110, height:50}} />
                </View> 
            ),
            headerRight: (
                <ButtonView activeOpacity={0.7} onPress={navigation.getParam('headerRightButton')} style={{width: 70, height: 50, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{color: '#f06d12', fontSize: 16, fontWeight: 'bold'}}>SKIP</Text>
                </ButtonView>
            )
        }
    }

    componentDidMount(){
        this.props.navigation.setParams({ headerRightButton: this.skipExtras });
        this._getAllPackages();
    }

    skipExtras = () => {
        this.goNext("ActivityInvoice");
    };

    _getAllPackages=()=> {
        Util.showLoader(this);
        this.props.getAllPackgesRequest((data)=>{
            if(data) Util.hideLoader(this);
        });
    }

    _selectActivity = (item)  => {
        if( item && item.product_name && item.category_id ) {
            this._addAccessories(item);
        } else {
            this._addPackage(item);
        }
    }

    _addPackage(item) {
        let obj = {
            'package_id': item.id,
            'package_price': item.price,
            'quantity': 1
        }
        if( this.state.extras && this.state.extras.packages.length > 0 ) {
            let loop = 0;
            let packages = this.state.extras.packages;
            packages.map(packageItem => {
                loop++;
                if( packageItem.package_id == item.id ) {
                    this.state.extras.packages.splice(loop-1, 1);
                    return false;
                }
                else if(packages.length == loop ) {
                    this.state.extras.packages.push(obj);
                    return false;
                }
            })
        } else {
            this.state.extras.packages.push(obj);
        }
        this.setState({indexChecked: item.id});
    }

    _addAccessories(item) {
        let obj = {
            'product_id': item.id,
            'product_price': item.sale_price,
            'quantity': 1
        }
        if( this.state.extras && this.state.extras.products.length > 0 ) {
            let loop = 0;
            let products = this.state.extras.products;
            products.map(productItem => {
                loop++;
                if( productItem.product_id == item.id ) {
                    this.state.extras.products[loop-1].quantity = products[loop-1].quantity + 1;
                    return false;
                }
                else if(products.length == loop ) {
                    this.state.extras.products.push(obj);
                    return false;
                }
            })
        } else {
            this.state.extras.products.push(obj);
        }
        this.setState({indexChecked: item.id});
    }

    renderPackages = (item, index) => {
        let packageSelected, isAdded = "ADD", isOdd = null;
        if( this.packagesIndex == index+1 && (this.packagesIndex % 2) == 1 ) {
            isOdd = {flex: 0.5, marginRight: 25};
        }
        for(let x = 0; x < this.state.extras.packages.length ; x++) {
            if(this.state.extras.packages[x].package_id == item.id) {
                packageSelected = true;
                isAdded = "ADDED TO CART";
                break;
            }
            else {packageSelected = false;}
        }
        return (
          <TouchableOpacity onPress={()=> this._selectActivity(item) } style={[styles.packageContainer, isOdd]}>
            <View style={packageSelected?styles.selectedPackageImageContainer:styles.packagesImageContainer}>
                <RNImage source={{uri:this.state.imageURL+item.image}} style={{flex: 1, borderRadius:8, resizeMode: 'cover'}} />
            </View>
            <View style={packageSelected?styles.selectedActivityContainerSelected:styles.activityContainerSelected} >
                <Text style={packageSelected?styles.selectedActivityText:styles.activityText}>{item.package_name}</Text>
                <Text style={packageSelected?styles.selectedPackagePriceText:styles.packagePriceText}>{`AED ${item.price}`}</Text>
                <TouchableOpacity style={packageSelected?styles.selectedAddPackageContainer:styles.addPackageContainer} onPress={()=> this._addPackage(item)}>
                    <Text style={packageSelected?styles.selectedAddPackageButtonText:styles.addPackageButtonText}>{isAdded}</Text>
                </TouchableOpacity>
            </View>
          </TouchableOpacity>
        )
    }

    renderAccessories = (item, index) => {
        let selectedPackage, isAdded = "ADD", quantity = 0, isOdd = null;
        if( this.productsIndex == index+1 && (this.productsIndex % 2) == 1 ) {
            isOdd = {flex: 0.5, marginRight: 25};
        }
        for(let x = 0; x < this.state.extras.products.length ; x++) {
            if(this.state.extras.products[x].product_id == item.id) {
                selectedPackage = true;
                isAdded = "ADDED TO CART";
                quantity = this.state.extras.products[x].quantity;
                break;
            }
            else {
                isAdded = "ADD";
                selectedPackage = false;
            }
        }
        return (
          <TouchableOpacity onPress={()=> this._selectActivity(item) } style={[styles.packageContainer, isOdd]}>
            {quantity > 0 ? (
                <View style={styles.quantityContainer}><Text style={{fontSize: 16, color: '#fff'}}>{quantity}</Text></View>
            ):null}
            <View style={styles.packagesImageContainer}>
                <RNImage source={{uri:this.state.imageURL+item.image}} style={{flex: 1, borderRadius:8, resizeMode: 'contain'}} />
            </View>
            <View style={[styles.activityContainerSelected, selectedPackage?{backgroundColor: '#F06E11'}:null]} >
                <Text style={[styles.activityText, selectedPackage?{color:'#fff'}:null]}>{item.product_name}</Text>
                <Text style={[styles.packagePriceText, selectedPackage?{color:'#fff'}:null]}>{`AED ${item.sale_price}`}</Text>
                <TouchableOpacity style={[styles.addPackageContainer, selectedPackage?{backgroundColor:'#fff'}:null]} onPress={()=> this._addAccessories(item)}>
                    <Text style={[styles.addPackageButtonText,selectedPackage?{color: '#F06E11'}:null]}>{isAdded}</Text>
                </TouchableOpacity>
            </View>
          </TouchableOpacity>
        )
    }

    gotoInvoice() {
        console.log('this.sttet', this.state.extras)
        let {packages, products} = this.state.extras;
        if( (products && products.length > 0) || (packages && packages.length > 0)) {
            Util.showLoader(this);
            this.state.extras.activity_id = this.props.navigation.getParam('activity_id');
            this.props.createActivityPackageRequest(this.state.extras, data=>{
                if(data){
                    this.goNext("ActivityInvoice");
                } else {
                    Util.topAlertError("Services not available");
                }
                Util.hideLoader(this);
            });
        } else {
            this.goNext("ActivityInvoice");
        }
    }

    goNext = (page) => {
        this.props.navigation.navigate(page);
    };

    _renderEmptyComponent =  (isLoading)=> {
        if (isLoading) {
          return null;
        }
        return (
         <EmptyStateText/> 
        );
    };

    render(){
        const { loading } = this.state;
        const { packages, products } = this.props.addedPackages ? this.props.addedPackages :{};
        this.productsIndex = products && products.length ? products.length : 0;
        this.packagesIndex = packages && packages.length ? packages.length : 0;

        return(
            <View style={[styles.container, styles.sectionContainer]}>
                <View style={styles.packageAndAccessoriesSection}>
                    <Text style={styles.sectionHeading}>VALUE ADDED PACKAGES</Text>
                    <FlatList
                        data={packages}
                        numColumns={2}
                        renderItem={({item, index}) => this.renderPackages(item, index)}
                        keyExtractor={item => item.id}
                        ListEmptyComponent={()=>this._renderEmptyComponent(loading)}
                        refreshing={loading}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                <View style={{borderColor: 'silver', borderWidth: StyleSheet.hairlineWidth}}></View>
                <View style={styles.packageAndAccessoriesSection}>
                    <Text style={styles.sectionHeading}>ACCESSORIES</Text>
                    <FlatList
                        data={products}
                        numColumns={2}
                        renderItem={({item, index}) => this.renderAccessories(item, index)}
                        keyExtractor={item => item.id}
                        ListEmptyComponent={()=>this._renderEmptyComponent(loading)}
                        refreshing={loading}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                {
                    this.state.extras.packages.length > 0 || this.state.extras.products.length > 0 ? (
                        <TouchableOpacity style={styles.floatingNextButton} onPress={()=> this.gotoInvoice()}>
                            <Icon style={styles.fowardArrow} name="chevron-right" />
                        </TouchableOpacity>
                    ) : null
                }
                <Loader loading={this.state.loading} />
            </View>
        );
    }
}
const mapStateToProps = (data) => {
    const { user, activities } = data;
    return {
        user: user.data,
        activities: activities.data,
        addedPackages: activities.addedServices
    }
}
const actions = { getAllPackgesRequest, createActivityPackageRequest };

export default connect(
  mapStateToProps,
  actions
)(PackagesAccessoriesComponent);

