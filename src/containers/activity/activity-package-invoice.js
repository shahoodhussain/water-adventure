import React, { Component } from 'react'
import { View, Text,TextInput, StyleSheet, Dimensions, FlatList, KeyboardAvoidingView } from 'react-native';
import { getCartItemsRequest, applyVoucerRequest, checkoutRequest, updateCardRequest, removeCartRequest } from "../../actions/ActivitiesAction";
import { userSignOutRequest } from "../../actions/UserActions";
import { connect } from "react-redux";
import { ButtonView,Image,Loader,EmptyStateText } from "../../components";
import styles from '../../assets/stylesheets/styles.js';
import Util from "../../util";
import { InvoicePayment, SubmitButton } from "./sub-component/invoice-payment-details";
import { InvoiceItem } from "./sub-component/render-list-items";
import _ from "lodash";

class PackageInvoiceActivity extends Component {

    static navigationOptions = ({ navigation }) =>  {
        return { 
            headerTitle:( 
                <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
                    <Image
                    resizeMode="center"
                    source={require('../../assets/images/logo.png')}
                    style={{width:110, height:50}}
                    />
                </View> 
            ),
        }
    }

    state = {
        loading: false,
        language: '',
        imageURL : 'http://dev70.onlinetestingserver.com/water-adventure-new/public/uploads/',
        indexChecked: '0',
        deviceState: this.getOrientation(),
        voucherCode: '',
        selectedActivity: {},
        cashAmount: 0,
        cardAmount: 0,
        voucherTotal: null,
        cartDetails: {
            "packages": null,
            "products": null,
            "totals": null,
            "selectedActivity": null,
            "activity_id": null
        }
    };

    componentDidMount() {
        Util.showLoader(this);
        this.props.getCartItemsRequest((data)=>{
            if(data) {
                this.setState({cartDetails: data, voucherTotal: data.totals, selectedActivity: data.activities})
                this.state.cartDetails.selectedActivity = data.activities;
                this.state.cartDetails.activity_id = this.props.navigation.getParam('activity_id');
            }
            else {
                Util.topAlertError("Unable to generate invoice");
            };
            Util.hideLoader(this);
        });
        Dimensions.addEventListener( 'change', () => {
            let newOrentation = this.getOrientation();
            this.setState({deviceState: newOrentation})
        });
    }

    getOrientation() {
        const inputState = Dimensions.get('window').width < Dimensions.get('window').height ? 'PORTRAIT' : 'LANDSCAPE';
        return inputState;
    }

    _renderEmptyComponent =  (isLoading)=> {
        if (isLoading) {
          return null;
        }
        return (
         <EmptyStateText/> 
        );
    };

    renderItems = (item) => {
        let state = this.state.deviceState=='PORTRAIT' ? true : false;
        return (
            <InvoiceItem currentState={state} currentItem={item} handleQuantityChange={(id, type)=>this.handleQuantity(id,type)} removeItem={(id, itemType)=>this.removeCartItem(id, itemType)}/>
        )
    }

    handleQuantity = (id, type) => {
        let products = this.state.cartDetails.products;
        if( type == "REMOVE" ) {
            for(let x = 0; x < products.length; x++) {
                if( products[x].id == id ) {
                    if( products[x].quantity <= 0 ) {
                        Util.topAlertError("Product quantity can not be less then zero")
                    } else {
                        let updatedQty = this.state.cartDetails.products[x].quantity - 1;
                        let cart_id = this.state.cartDetails.products[x].id;
                        this.updateCart(updatedQty, cart_id)
                    }
                }
            }
        } else {
            for(let x = 0; x < products.length; x++) {
                if( products[x].id == id ) {
                    let updatedQty = this.state.cartDetails.products[x].quantity + 1;
                    let cart_id = this.state.cartDetails.products[x].id;
                    this.updateCart(updatedQty, cart_id)
                }
            }
        }
        this.setState({});
    }

    removeCartItem = (id, itemType) => {
        let {products, packages} = this.state.cartDetails;
        if( itemType == "accessory" ) {
            for(let x = 0; x < products.length; x++) {
                if( products[x].id == id ) {
                    let cart_id = products[x].id;
                    this.deleteCart(cart_id);
                    this.state.cartDetails.products.splice(x, 1);
                    break;
                }
            }
        } else {
            for(let x = 0; x < packages.length; x++) {
                if( packages[x].id == id ) {
                    let cart_id = packages[x].id;
                    this.deleteCart(cart_id);
                    this.state.cartDetails.packages.splice(x, 1);
                    break;
                }
            }
        }
        this.setState({})
    }

    updateCart(quantity, cart_id) {
        Util.showLoader(this);
        let obj = {
            'cart_type': 'accessory',
            'cart_quantity': quantity,
            'cart_id': cart_id,
        }
        this.props.updateCardRequest(obj, data=>{
            if(data) {
                this.setState({ cartDetails: data })
            } else {
                Util.topAlertError("Services not available");
            }
            Util.hideLoader(this);
        });
    }

    deleteCart(cart_id) {
        Util.showLoader(this);
        let obj = {
            'cart_id': cart_id,
        }
        this.props.removeCartRequest(obj, data=>{
            if(data) {
                this.setState({ cartDetails: data })
            } else {
                Util.topAlertError("Services not available");
            }
            Util.hideLoader(this);
        });
    }

    applyVoucher() {
        if(!_.isEmpty(this.state.voucherCode)) {
            Util.showLoader(this);
            let payload = { 'code': this.state.voucherCode };
            this.props.applyVoucerRequest(payload,data=>{
                if(data) {
                    this.setState({voucherTotal: data.totals})
                }
                else {
                    Util.topAlertError("Voucher code is not valid");
                };
                Util.hideLoader(this);
            });
        } else {
            Util.topAlertError("Please enter voucher code");
        }
    }

    submit() {
        let cash = parseInt(this.state.cashAmount);
        let card = parseInt(this.state.cardAmount);
        if( (card+cash) == this.state.cartDetails.totals.grand_total ) {
            Util.showLoader(this);
            let payload = {
                "code": this.state.voucherCode
            }
            this.props.checkoutRequest(payload,booking=>{
                if(booking) {
                    this.props.navigation.navigate('OrderReceipt', {'booking_details': booking})
                }
                else {
                    Util.topAlertError("Checkout Error");
                };
                Util.hideLoader(this);
            });
        } else {
            Util.topAlertError("Destribution should be equals to total amount");
        }
    }

    render() {
        const { packages, products, totals } = this.state.cartDetails ? this.state.cartDetails:{};
        const paymentDetails = totals && typeof totals != "undefined" ? totals : this.state.voucherTotal;
        let allItems = [];
        if(this.state.selectedActivity && this.state.selectedActivity.length > 0) {
            let {id, image, item_type, total, title} = this.state.selectedActivity[0];
            let obj = {
                'id': id,
                'image': image,
                'item_type': item_type,
                'total': total,
                'title': title
            }
            allItems.push(obj)
        }
        if(packages && packages.length > 0) {
            packages.map(item=> {
                allItems.push(item)
            })
        }
        if(products && products.length > 0) {
            products.map(item=>{
                allItems.push(item)
            })
        }
        return (
            <KeyboardAvoidingView style={styles.container} behavior={this.state.deviceState=="LANDSCAPE" ? "height":"none"} >
                <View style={{flex: 6}}>
                    <Text style={[styles.sectionHeading, {marginLeft: 20}]}>INVOICE</Text>
                    <View style={[styles.mainContainer, {flex: 1}]}>
                        <View style={styles.invoiceBox}>
                            <FlatList
                                data={allItems}
                                renderItem={({item}) => this.renderItems(item)}
                                keyExtractor={item => item.id}
                                ListEmptyComponent={()=>this._renderEmptyComponent(this.state.loading)}
                                refreshing={this.state.loading}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                        <View style={{borderColor: 'silver', borderWidth: StyleSheet.hairlineWidth, marginLeft: 20}}></View>
                        <View style={styles.invoiceSlip}>
                            <Text style={styles.voucherHeading}>Voucher No</Text>
                            <View style={styles.voucherBox}>
                                <TextInput placeholder="215484" style={[styles.input, styles.voucherInput]} onChangeText={(text)=>this.setState({voucherCode: text})}/>
                                <ButtonView style={styles.applyVoucherButton} onPress={()=>this.applyVoucher()}>
                                    <Text style={{color: '#fff'}}>APPLY</Text>
                                </ButtonView>
                            </View>

                            <InvoicePayment total={paymentDetails}/>

                            <View style={styles.packageRow}>
                                <View style={styles.detailTop}>
                                    <Text style={styles.voucherHeading}>Pay Cash</Text>
                                    <View style={styles.voucherBox}>
                                        <TextInput placeholder="Cash Amount" onChangeText={(amount)=>this.setState({cashAmount: amount})} style={[styles.input, styles.voucherInput]} keyboardType="number-pad"/>
                                    </View>
                                </View>
                                <View style={{flex: 0.05}}></View>
                                <View style={styles.detailTop}>
                                    <Text style={styles.voucherHeading}>Pay by Card</Text>
                                    <View style={styles.voucherBox}>
                                        <TextInput placeholder="Card Amount" onChangeText={(amount)=>this.setState({cardAmount: amount})} style={[styles.input, styles.voucherInput]} keyboardType="number-pad"/>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>

                </View>
                <SubmitButton submit={()=> this.submit()} title="BOOK NOW"/>
                <Loader loading={this.state.loading} />
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (data) => {
    // console.log('mapppppp', data)
    const { user, activities } = data;
    return {
        user: user.data,
        activities: activities.data,
        addedPackages: activities.packages
    }
}
const actions = { getCartItemsRequest, userSignOutRequest, applyVoucerRequest, checkoutRequest, updateCardRequest, removeCartRequest };

export default connect(
  mapStateToProps,
  actions
)(PackageInvoiceActivity);

