import React, { Component } from 'react'
import { View, Text,Image as RNImage, Dimensions, ImageBackground, FlatList, BackHandler } from 'react-native';
import { getAllPackgesRequest, finishCheckoutRequest } from "../../actions/ActivitiesAction";
import { userSignOutRequest } from "../../actions/UserActions";
import { connect } from "react-redux";
import { Image,Loader,EmptyStateText } from "../../components";
import styles from '../../assets/stylesheets/styles.js';
import { ActivityOrderDetails, TotalSection } from "./sub-component/order-details";
import { SubmitButton } from "./sub-component/invoice-payment-details";
import { StackActions } from 'react-navigation';

class OrderDetails extends Component {

    static navigationOptions = ({ navigation }) =>  {
        return{  
        headerTitle:( 
            <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
                <Image
                resizeMode="center"
                source={require('../../assets/images/logo.png')}
                style={{width:110, height:50}}
                />
            </View> 
            )
        }
    }

    state = {
        loading: false,
        language: '',
        imageURL : 'http://dev70.onlinetestingserver.com/water-adventure/public/uploads/',
        indexChecked: '0',
        deviceState: this.getOrientation(),
        booking_details: {}
    };

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        Dimensions.addEventListener( 'change', () => {
            let newOrentation = this.getOrientation();
            this.setState({deviceState: newOrentation})
        });
        let navigationParams = this.props.navigation.getParam('booking_details');
        this.setState({booking_details: navigationParams})
    }
    
    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.printSlip(); // works best when the goBack is async
        return true;
    }

    getOrientation() {
        const inputState = Dimensions.get('window').width < Dimensions.get('window').height ? 'PORTRAIT' : 'LANDSCAPE';
        return inputState;
    }

    _renderEmptyComponent =  (isLoading)=> {
        if (isLoading) {
          return null;
        }
        return (
         <EmptyStateText/> 
        );
    };

    printSlip() {
        this.props.finishCheckoutRequest();
        this.props.navigation.dispatch(StackActions.popToTop());
    }

    renderItems = (item,index) => {
        let {id, title, price, quantity} = item;
        return (
            <View style={styles.receiptRow} key={id}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    {/* <Text style={styles.receiptText}>ITEM </Text> */}
                    <Text style={styles.receiptText}>{++index})  {title}</Text>
                </View>
                <Text style={{fontSize: 16}}>QTY {quantity}   --   </Text>
                <Text style={{fontSize: 16}}>AED {price}</Text>
            </View>
        )
    }

    render() {
        let booking_details = this.state.booking_details ? this.state.booking_details:'';
        console.log('booking details', booking_details)
        return (
            <View style={[styles.container, {backgroundColor: '#fff'}]}>
                <View style={styles.mainContainer}>
                    <ActivityOrderDetails booking_object={booking_details}>
                        <FlatList
                            data={booking_details.items}
                            renderItem={({item,index}) => this.renderItems(item,index)}
                            keyExtractor={item => item.id}
                            ListEmptyComponent={()=>this._renderEmptyComponent(this.state.loading)}
                            refreshing={this.state.loading}
                            showsVerticalScrollIndicator={false}
                        />
                    </ActivityOrderDetails>
                    <View style={styles.receiptBox}>
                        <Text style={[styles.sectionHeading, {color: '#000'}]}>ORDER RECEIPT</Text>
                        <ImageBackground style={styles.receiptDetails} resizeMode="stretch" source={require('./../../assets/images/receipt_slip.png')}>
                            <View style={styles.receiptHeadingBox}>
                                <Text style={styles.receiptHeading}>***   RECEIPT   ***</Text>
                                <Text style={styles.receiptText}>Order#: {booking_details ? booking_details.order_no : '0'}       {booking_details ? booking_details.order_date:'-'} - {booking_details ? booking_details.order_time:'-'}</Text>
                            </View>
                            <Text style={{alignSelf: 'center'}}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                            <FlatList
                                data={booking_details.items}
                                renderItem={({item,index}) => this.renderItems(item,index)}
                                keyExtractor={item => item.id}
                                ListEmptyComponent={()=>this._renderEmptyComponent(this.state.loading)}
                                refreshing={this.state.loading}
                                showsVerticalScrollIndicator={false}
                            />
                            <Text style={{alignSelf: 'center'}}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                            <TotalSection booking_details={booking_details}/>
                            <View style={{marginBottom: 40}}></View>
                        </ImageBackground>
                    </View>
                </View>
                <SubmitButton submit={()=> this.printSlip()} title="CONTINUE"/>
                <Loader loading={this.state.loading} />
            </View>
        )
    }
}

const mapStateToProps = (data) => {
    const { user, activities } = data;
    return {
        user: user.data,
        activities: activities.data,
    }
}
const actions = { getAllPackgesRequest, userSignOutRequest, finishCheckoutRequest };

export default connect(
  mapStateToProps,
  actions
)(OrderDetails);

