import React from 'react';
import { View,Alert,ImageBackground,Picker,ScrollView, Text,TextInput,Image as RNImage,TouchableOpacity, StyleSheet } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import _ from "lodash";
import { ButtonView,Image,Loader,EmptyStateText } from "../../components";
import Util from "../../util";
import { connect } from "react-redux";
import { userSignOutRequest} from "../../actions/UserActions";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ModalSelector from 'react-native-modal-selector';
import DatePicker from 'react-native-datepicker'



class ActivityDetailPageComponent extends React.Component {

    state = {
        loading: false,
        language: '',
        textInputValue: '',
        activity_attribute_id:'',
        email : '',
        booking_date :'',
        time_from:'',
        no_of_person_per_jetski:'',
        age:'',
        center_id:''
    };
    
    static navigationOptions = ({ navigation }) =>  {
        return{  
        headerTitle:( 
            <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
            <Image
                resizeMode="center"
                source={require('../../assets/images/logo.png')}
                style={{width:110, height:50}}
            />
            </View> 
            ),
            headerRight: (
              <ButtonView activeOpacity={0.7} onPress={navigation.getParam('headerRightButton')}   style={styles.logoutContainer}>
                <RNImage source={require('../../assets/images/logout_icon.png')} style={styles.play} />  
              </ButtonView>
            )
        }
    }

    componentDidMount(){
        this.props.navigation.setParams({ headerRightButton: this._userSignOutRequest });
        const { navigation } = this.props;
        const data = navigation.getParam('item');
        console.log('Navigation dataaaaaaaaaaaaaaaaaaaaa', data);

    }

    _userSignOutRequest = () => {
        Alert.alert(
          "Logout",
          "Are you sure?",
          [
            {
              text: "Yes",
              onPress: () => {
                Util.showLoader(this);
                this.props.userSignOutRequest(() => {
                  Util.hideLoader(this);
                  setTimeout(() => {
                    this.props.navigation.navigate("Login");
                  }, 500);
                });
              }
            },
            { text: "No", style: "cancel" }
          ],
      
          { cancelable: true }
        );
      };

    render(){
        const { navigation ,user} = this.props;
        const data = navigation.getParam('item');
        var attributes = (data.get_attributes);
        const peoples = [];
        const time_from = []
        const timings = data.timings.split(',');
        if(timings.length>0){
            for(var i=0;i<timings.length; i++){
                time = { key: i, label: timings[i] }
                time_from.push(time);
            }       
        }else{
            time = {key:0,label:"No values found"};
            time_from.push(time);
        }

        
        
        if(attributes.length>0){
            for(var i=0;i<attributes.length; i++){
                attribute = { key: attributes[i].id, label: attributes[i].duration+" mins, AED "+attributes[i].price+" (min "+attributes[i].people+" - up to "+attributes[i].max_people+" persons)" }
                peoples.push(attribute);
            }       
        }else{
            attribute = {key:0,label:"No values found"};
            peoples.push(attribute);
        }
        
        
        return(
            <KeyboardAwareScrollView
            style={{ backgroundColor: '#fff' }}
            resetScrollToCoords={{ x: 0, y: 0 }}
            contentContainerStyle={styles.container}
            scrollEnabled={true}> 
                <ImageBackground source={require('../../assets/images/detail.png')} style={styles.play}>
                    <View style={styles.detailTop}></View>
                    <View style={styles.detailMiddle}>
                        <ImageBackground source={require('../../assets/images/order_form_background.png')} style={styles.play} imageStyle={{ borderRadius: 8}}>
                            <View style={{flex:0.5,flexDirection:'row',alignItems:'flex-start',paddingTop:10,paddingLeft:20}}>
                                <Text style={styles.topHeading}>{data.activity_name}</Text>
                            </View>
                            <View style={{height:400,paddingTop:10}}  >  
                            <View style={styles.formGroup}>   
                                <View style={{flex:1,alignItems:'flex-start',justifyContent:'center'}}>
                                    <Text style={styles.labelTop}>No of people</Text>
                                    <ModalSelector
                                        data={peoples}
                                        initValue="No Of Persons"
                                        supportedOrientations={['landscape']}
                                        accessible={true}
                                        overlayStyle={{ flex: 1, padding: '5%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}
                                        style={styles.formInputDetail2}
                                        selectStyle={styles.selectStyle}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option)=>{ this.setState({textInputValue:option.label,activity_attribute_id:option.key})}}>
                                        
                                        <TextInput style={styles.input2} placeholder='No of People' value={this.state.textInputValue} ></TextInput>

                                    </ModalSelector>
                                

                                </View>

                                <View style={{flex:1,alignItems:'flex-start',justifyContent:'center'}}>
                                <Text style={styles.labelTop}>Email</Text> 
                                <View style={styles.formInputDetail}>
                                    <TextInput style={styles.input2} selectionColor='#000' returnKeyType="next" onSubmitEditing={()=>this.no_of_jetski.focus()}  placeholder='Email Address'   onChangeText={email => this.setState({email})}></TextInput>
                                </View>
                                </View>
                            </View>

                            <View style={styles.formGroup}>   
                                

                                <View style={{flex:1,alignItems:'flex-start'}}>
                                <Text style={styles.labelTop}>No of JetSki</Text> 
                                <View style={styles.formInputDetail}>
                                    <TextInput style={styles.input2} keyboardType={'numeric'} returnKeyType="next" onSubmitEditing={()=>this.no_of_person_per_jetski.focus()} ref={(input)=>this.no_of_jetski = input} selectionColor='#000' placeholder='No of Jetski'   onChangeText={no_of_jetski => this.setState({no_of_jetski})}></TextInput>
                                </View>
                                </View>

                                <View style={{flex:1,alignItems:'flex-start'}}>
                                <Text style={styles.labelTop}>Persons</Text> 
                                <View style={styles.formInputDetail}>
                                    <TextInput style={styles.input2} keyboardType={'numeric'} returnKeyType="next" ref={(input)=>this.no_of_person_per_jetski = input} onSubmitEditing={()=>this.age.focus()} selectionColor='#000' placeholder='Persons Per Ride'   onChangeText={no_of_person_per_jetski => this.setState({no_of_person_per_jetski})}></TextInput>
                                </View>
                                </View>

                            </View>

                            <View style={styles.formGroup}>   
                                <View style={{flex:1,alignItems:'flex-start',marginTop:10}}>
                                <Text style={styles.labelTop}>Age</Text> 
                                <View style={styles.formInputDetail}>
                                    <TextInput style={styles.input2} ref={(input)=>this.age = input} keyboardType={'numeric'} returnKeyType="next"  selectionColor='#000' placeholder='Age'   onChangeText={age => this.setState({age})}></TextInput>
                                </View>
                                </View>

                                <View style={{flex:1,alignItems:'flex-start',marginTop:10}}>
                                <Text style={styles.labelTop}>Center</Text> 
                                <View style={styles.formInputDetail}>
                                    <TextInput style={styles.input2} selectionColor='#000' placeholder='Center'  value={data.center.center_name}   onChangeText={center_id => this.setState({center_id})}></TextInput>
                                </View>
                                </View>
                            </View>

                            <View style={styles.formGroup}>   
                                <View style={{flex:1,alignItems:'flex-start',marginTop:20}}>
                                <Text style={styles.labelTop}>Date</Text> 
                                <View style={styles.formInputDetail}>
                                <DatePicker
                                style={styles.input}
                                date={this.state.booking_date}
                                mode="date"
                                placeholder="Booking date"
                                format="YYYY-MM-DD"
                                minDate="2019-05-01"
                                maxDate="2020-12-31"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    //marginLeft: 10,
                                    fontSize:6,
                                    borderColor:'#fff',
                                    alignItems:'flex-start'
                                }
                                
                                }}
                                
                                onDateChange={(booking_date) => {this.setState({booking_date: booking_date})}}
                                />
                                    {/* <TextInput style={styles.input} selectionColor='#000' placeholder='Email Address' ref={(input)=>this.dateInput = input}  autoFocus={true} onChangeText={booking_date => this.setState({booking_date})}></TextInput> */}
                                </View>
                                </View>

                                <View style={{flex:1,alignItems:'flex-start',marginTop:20}}>
                                <Text style={styles.labelTop}>Time Slot</Text> 
                                
                                <ModalSelector
                                        data={time_from}
                                        initValue="Time Slot"
                                        supportedOrientations={['landscape']}
                                        accessible={true}
                                        overlayStyle={{ flex: 1, padding: '5%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}
                                        style={styles.formInputDetail}
                                        selectStyle={styles.selectStyle}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option)=>{ this.setState({time_from:option.label})}}>
                                        
                                        <TextInput style={styles.input2} placeholder='Time Slot' value={this.state.time_from} ></TextInput>

                                    </ModalSelector>
                                
                                </View>
                            </View>

                            

                            

                            <View style={styles.formGroup}>   
                                <View style={{flex:1,alignItems:'flex-start',marginTop:30}}>
                                <Text style={styles.labelTop}>Full Name</Text> 
                                <View style={styles.formInputDetail}>
                                    <TextInput style={styles.input2} returnKeyType="next" ref={(input)=>this.full_name = input}  selectionColor='#000' placeholder='Full Name'   onChangeText={full_name => this.setState({full_name})}></TextInput>
                                </View>
                                </View>

                                <View style={{flex:1,alignItems:'flex-start',marginTop:30}}>
                                    <Text style={styles.labelTop}>Phone</Text> 
                                    <View style={styles.formInputDetail3}>
                                        <Image source={require('../../assets/images/flag_icon.png')} style={styles.iconImage2} />
                                        <Text style={{marginLeft:'2%'}}>+971</Text>
                                        <View style={{marginLeft: 10, marginRight: 10, width: StyleSheet.hairlineWidth, backgroundColor: '#000', height: 20}} />
                                        <TextInput style={styles.input2} keyboardType={'numeric'} ref={(input)=>this.phone = input} selectionColor='#000'    onChangeText={phone => this.setState({phone})}></TextInput>
                                    </View>
                                </View>
                            </View>
                            <View style={{height:25}}></View>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.detailBottom}>
                        <ButtonView activeOpacity={0.5}   style={styles.buttonContainer}>
                            <Image source={require('../../assets/images/button.png')} style={{width:'100%',height:60}} />  
                            <Text style={styles.loginButtonText}>NEXT</Text> 
                        </ButtonView>
                    </View>
                    <View style={styles.detailFooter}></View>
                </ImageBackground>
                <Loader loading={this.state.loading} />
            </KeyboardAwareScrollView>
        );
    }
}
const mapStateToProps = ({user }) => ({
    user:user.data
});
const actions = {userSignOutRequest };

export default connect(
  mapStateToProps,
  actions
)(ActivityDetailPageComponent);

