/**
 * Created by faraz ali on 13/05/2019.
 */

import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Linking,
  Platform
} from "react-native";
import styles from "../../assets/stylesheets/styles.js";
//import SectionedMultiSelect from "react-native-sectioned-multi-select";
import DatePicker from "react-native-datepicker";
import Util from "../../util";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { connect } from "react-redux";
import { createClassesRequest ,getClassTypesRequest,getAllClassesRequest} from "../../actions/ClassesAction";
import _ from "lodash";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {  Loader } from "../../components";
import moment from "moment";


class ScheduleFormComponent extends Component {
  constructor(props) {
    super(props);
    const {userData,classesData}=this.props;
    console.log("constructor calls");
    console.log(classesData,"just check ")

  
    this.state = {
    selectedItems: "",
    selectedEquipments:"",
    date:"",
    startTime:null,
    endTime:null,
    lat:"",
    lng:"",
    location:"",
    hideList: false,
    stdNum:"",
    items:[
      {
      name: "Training Equipments",
      id: 0,
      // these are the children or 'sub items'
      children:userData.trainer_equipments
    }
  ],
  name:userData.name,
  loading: false,
  classType:[
    {
      name: "Class Type",
      id: 0,
      // these are the children or 'sub items'
      children:classesData
    }
  ]

    };

  }
  types=[
    {
      name: "Class Type",
      id: 0,
      // these are the children or 'sub items'
      children:[]
    }
  ]
  componentDidMount(){
    console.log("didmount calls")

    Util.showLoader(this);
    this.props.getClassTypesRequest((data)=>{
   this.types=[
    {
      name: "Class Type",
      id: 0,
      // these are the children or 'sub items'
      children:data
    }
   ]
      Util.hideLoader(this);  
  });
  }
  onSelectedItemsChange = selectedItems => {
    console.log(selectedItems,"selec")
    this.setState({  selectedItems });
  };
  openCalendar = () => {
    if (Platform.OS === "ios") {
      Linking.openURL("calshow:");
    } else {
      Linking.openURL("content://com.android.calendar/time/");
    }
  };
  _Submit=()=>{
    const currentTime=new Date().toTimeString();
    console.log(currentTime,"currentTime");
    const {date,startTime,endTime,stdNum,selectedItems,location,selectedEquipments}=this.state;

if(_.isEmpty(date)){
  Util.topAlertError("Select Date");
}
else if(_.isEmpty(startTime)){
  Util.topAlertError("Select start time");
}
else if(_.isEmpty(endTime)){
  Util.topAlertError("Select end time");
}
else if(_.isEmpty(stdNum)){
  Util.topAlertError("Enter number of students")
}
else if(_.isEmpty(selectedItems)){
  Util.topAlertError("Select class type")
}
else if(_.isEmpty(location)){
  Util.topAlertError("Select location")
}
// else if(_.isEmpty(selectedEquipments)){
//   Util.topAlertError("Select Equipments")
// }
else {
  const payload={
    
    class_type_id:this.state.selectedItems.toString(),
    std_num:this.state.stdNum,
   location:this.state.location,
   lat:this.state.lat,
   lng:this.state.lng,
   start_class_time: `${this.state.date} ${this.state.startTime}`,
   end_class_time: `${this.state.date} ${this.state.endTime}`,
  }
Util.showLoader(this);
this.props.createClassesRequest(payload,(data)=>{
  Util.hideLoader(this);
  console.log(data);
  if(data){
     this.props.getAllClassesRequest();
 this.props.navigation.goBack();

  }
 // this.props.getAllClassesRequest();
 // this.props.navigation.goBack();
});
}
   
}
onSelectedequipmentsChange=selectedEquipments=>{
 console.log(selectedEquipments,"selec")
  this.setState({  selectedEquipments });
}
componentWillReceiveProps(props){
  const {classesData}=this.props;
 this.setState({
  classType:[
    {
      name: "Class Type",
      id: 0,
      // these are the children or 'sub items'
      children:classesData
    }
  ]
 })
  
}
  render() {
console.log(this.state.classType,"classtype")

  const{loading}=this.state;
var minDate=Util.getCurrentDate();

    return (
      <KeyboardAwareScrollView
      enableOnAndroid
      keyboardShouldPersistTaps={"handled"}
     style={{backgroundColor:"#fff" }}
     showsVerticalScrollIndicator={false}>
      <ScrollView
        contentContainerStyle={{
          backgroundColor: "#fff",
          //  marginVertical: 20,
          paddingVertical: 20
          // flex: 1
        }}
      >
    
        <View style={Platform.OS==="android"?  styles.classTopView: styles.classNewContainer}>
          <Text style={styles.scheduleHeading}>Schedule a Class</Text>
        </View>
        
        
       {/* <View style={styles.scheduleFormFieldsView}>*/}
            <View style={{
              flex:1,
      //  backgroundColor:"green",
      //  flex: 1,
  //      justifyContent: "center",
   //    alignItems: "center"
            }}
            >

           

        
            <TextInput
            editable={false}
              password
              style={{
                flex: 1,
                paddingTop:20,
                paddingBottom:10,
                textAlign:"left",
                color: "#424242",
                marginLeft:20
              }}
              selectionColor="#900816"
              placeholder="Instructor Name"
              value={this.state.name}
            />
            <View style={{ 
          
              backgroundColor:"#000",
              height:0.4,
             marginHorizontal:30
             }}/>
             <View
             style={{
          
               paddingHorizontal:20,
               width: "100%",
               justifyContent: "center"
             }}
           >
             <DatePicker
             style={{ width: "105%" }}
             date={this.state.date}
             mode="date"
             placeholder="Date of Class"
             format="YYYY-MM-DD"
             minDate={minDate}
          //   maxDate={minDate}
             confirmBtnText="Confirm"
             cancelBtnText="Cancel"
             customStyles={{
               dateInput: {
               
                 borderColor: "#fff",
                 borderBottomColor: "#000",
                 borderBottomWidth: 0.5,

               },
               placeholderText: {
                 alignSelf:"flex-start",
            
            }
             }}
             onDateChange={date => {
               this.setState({ date: date });
             }}
           />
           
        </View>
        <View
        style={{
         
          paddingHorizontal:20,
          width: "100%",
          justifyContent: "center"
        }}
        >
        <DatePicker
        style={{ width: "105%" }}
        date={this.state.startTime}
        mode="time"
        placeholder="start time Of class"
        format="hh:mm A"
        // minDate="2019-05-01"
        // maxDate="2022-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateInput: {
          
            borderColor: "#fff",
            borderBottomColor: "#000",
            borderBottomWidth: 0.5,

          },
          placeholderText: {
            alignSelf:"flex-start",
       
       }
        }}
        onDateChange={(value1,value2) => {
          let endTime= (moment(value1,'hh:mm A').add(2,'hour').format('hh:mm A'));
console.log(value1,"starttime");
          this.setState({ startTime: value1 });
          this.setState({endTime})
        }}
      />
      </View>
      <View
      style={{
      
        paddingHorizontal:20,
        width: "100%",
        justifyContent: "center"
      }}
      >
      <DatePicker
      style={{ width: "105%" }}
      date={this.state.endTime}
      mode="time"
      placeholder="End time Of class"
      format="hh:mm A"
      // minDate="2019-05-01"
      // maxDate="2022-06-01"
      confirmBtnText="Confirm"
      cancelBtnText="Cancel"
      customStyles={{
        dateInput: {
        
          borderColor: "#fff",
          borderBottomColor: "#000",
          borderBottomWidth: 0.5,

        },
        placeholderText: {
          alignSelf:"flex-start",
     
     }
      }}
      onDateChange={(value1,value2) => {
        console.log(value1,"sndtime")
        this.setState({ endTime: value1 });
      }}
    />
    </View>
        </View>
  
      
           <TextInput
             password
             style={{
               flex: 1,
               paddingVertical: 10,
               textAlign:"left",
              marginHorizontal:20,
              paddingLeft: 0,      
             color: "#424242"
             }}
             selectionColor="#900816"
             placeholder="no of students"
             onChangeText={stdNum => this.setState({ stdNum })}
    keyboardType={"numeric"}
    maxLength={3}
           />
           <View style={{ 
          
            backgroundColor:"#000",
            height:0.5,
           marginHorizontal:30
           }}/>
         
     
             <View style={{ 
          
              backgroundColor:"#000",
              height:0.3,
             marginHorizontal:30,
             
             }}/>
      <GooglePlacesAutocomplete
      placeholder='Location'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed={this.state.hideList} 
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details = null) => {
        var hideList = ! this.state.hideList;
       console.log(details,"location")
        this.setState({
        lat: details.geometry.location.lat,
        lng: details.geometry.location.lng,
        hideList: false,
        placeSelected : true,
        location:details.formatted_address
      });
      //  this._gotoLocation();
      }
    }

      getDefaultValue={() => ''}

      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyBzZXCW8mTxjaGhvqtrxoosKfutY0xY67A',
        language: 'en', // language of the results
        types: '(cities)' // default: 'geocode'
      }}

      styles={{
        container:{
        //  alignItems:"center",
      //    justifyContent:"center"
        //  backgroundColor:"red"
        },
        textInputContainer: {
          borderBottomWidth: 0.3,
          borderBottomColor: "#000",
          backgroundColor:"#fff",
          width: '85%',
       
           marginLeft:20,
          
          borderTopColor:"#fff",
textAlign:"left"
        },
        description: {
          textAlign:"left",
          color: "#424242",
          fontWeight: 'bold'
        },
        textInput:{
          textAlign:"left"
        }
        
      }}

     // currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
    //  currentLocationLabel="Current location"
    //  nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={{
        // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
      }}
      GooglePlacesSearchQuery={{
        rankby: 'distance',
        types: 'food'
      }}

      filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
  
     textInputProps={{
      onFocus: () => this.setState({ hideList : false }),
      onBlur: () => this.setState({ hideList : true }),
    }}
     />
     <View
     style={{
       flex: 1,
       justifyContent: "center"
       //  backgroundColor: "red"
     }}
   >
     
     {/*<SectionedMultiSelect
        single
       items={this.types} 
       uniqueKey="id"
       subKey="children"
       selectText="Class Type"
       showDropDowns={true}
       expandDropDowns={true}
       readOnlyHeadings={true}
       onSelectedItemsChange={this.onSelectedItemsChange}
     //  onSelectedItemObjectsChange={(data)=>
      //   console.log(data,"object")
       // this.onSelectedItemsChange
    //   }
       selectedItems={this.state.selectedItems}
       hideSearch
       alwaysShowSelectText
       submitButtonColor="#f4c217"
       
     /> */}
   </View>
        <View
          style={{
            flex: 1,
            justifyContent: "center"
            //  backgroundColor: "red"
          }}
        >
          
          <SectionedMultiSelect
            items={this.state.items}
            uniqueKey="id"
            subKey="children"
            selectText="Training Equipments"
            showDropDowns={true}
            expandDropDowns={true}
            readOnlyHeadings={true}
            onSelectedItemsChange={
              this.onSelectedequipmentsChange
           // console.log(data,"onchange")
            }
          //   onSelectedItemObjectsChange={(data)=>
          //    console.log(data,"object")
          //  //  this.setState({selectedEquipments})
          //  }
            selectedItems={this.state.selectedEquipments}
            hideSearch
            alwaysShowSelectText
            submitButtonColor="#f4c217"
          />
        </View>
      
        <View style={styles.scheduleFormLowerView}>
          <TouchableOpacity
            activeOpacity={0.7}
 
          onPress={()=>this._Submit()}
            style={[styles.profileButton,{marginTop:10}]}
          >
            <Text style={{ fontSize: 18, color: "#000" }}>Schedule Now</Text>
          </TouchableOpacity>
        </View>
        <Loader loading={this.state.loading} />

      </ScrollView>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = ({ user,classes}) => ({
  userData: user.data,
  classesData:classes.classTypes

});
const actions = {createClassesRequest,getClassTypesRequest,getAllClassesRequest};

export default connect(
  mapStateToProps,
  actions
)(ScheduleFormComponent);
