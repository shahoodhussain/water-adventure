import React from 'react';
import { View,ImageBackground,ScrollView, Text,TextInput, StyleSheet, Dimensions } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import _ from "lodash";
import { ButtonView,Image,Loader } from "../../components";
import Util from "../../util";
import { connect } from "react-redux";
import { userSignOutRequest} from "../../actions/UserActions";
import { createActivityRequest, bookingDateRequest } from "../../actions/ActivitiesAction";
import ModalSelector from 'react-native-modal-selector';
import DatePicker from 'react-native-datepicker';
import CountryPicker from 'react-native-country-picker-modal'

class ActivityDetailPageComponent extends React.Component {

    state = {
        navigationData: null,
        loading: false,
        textInputValue: '',
        activity_attribute_id:'',
        email : '',
        booking_date :'',
        time_from:{
            'time_slot': null,
            'available_assets': null
        },
        no_of_person_per_jetski:'',
        age:null,
        center_id:'',
        no_of_jetski: null,
        center_id:'',
        center_name: '',
        full_name: '',
        phone: '',
        activity_price: '',
        max_people: 0,
        inputState: this.getOrientation(),
        timeSlotsAvailable: [],
        cca2: null,
        callingCode: null,
        extraGuideCharges: null,
    };

    
    static navigationOptions = ({ navigation }) =>  {
        return{  
        headerTitle:( 
            <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
            <Image
                resizeMode="center"
                source={require('../../assets/images/logo.png')}
                style={{width:110, height:50}}
            />
            </View> 
            )
        }
    }

    componentDidMount(){
        const { navigation } = this.props;
        const data = navigation.getParam('item');
        this.setState({ navigationData: data})
        // console.log('Navigation dataaaaaaaaaaaaaaaaaaaaa', data);
        Dimensions.addEventListener( 'change', () => {
            let newOrentation = this.getOrientation();
            this.setState({inputState: newOrentation})
        });
    }

    getOrientation() {
        const inputState = Dimensions.get('window').width < Dimensions.get('window').height ? 'PORTRAIT' : 'LANDSCAPE';
        return inputState;
    }

    submitForm = () => {
        const { email, no_of_jetski, no_of_person_per_jetski, booking_date, time_from, full_name, phone, age, activity_attribute_id, activity_price, navigationData, extraGuideCharges } = this.state;
        if(_.isEmpty(activity_attribute_id)) {
            Util.topAlertError("Please select ride");
        }
        else if(_.isEmpty(booking_date)) {
            Util.topAlertError("Booking Date is not selected");
        }
        else if (_.isEmpty(time_from.time_slot)) {
            Util.topAlertError("Select Available Time Slot");
        }
        else if (_.isEmpty(no_of_jetski)) {
            Util.topAlertError("Please select no of jetski ");
        }
        else if (_.isEmpty(no_of_person_per_jetski)) {
            Util.topAlertError("Please select no of jetski and no of people ");
        }
        else if (_.isEmpty(full_name)) {
            Util.topAlertError("Please enter your name");
        }
        else if (!Util.isValidName(full_name)) {
            Util.topAlertError("Input name is not valid");
        }
        else if (_.isEmpty(email)) {
            Util.topAlertError("Please enter your email");
        }
        else if (!Util.isEmailValid(email)) {
            Util.topAlertError("Email is invalid ");
        }
        // else if (_.isEmpty(age)) {
        //     Util.topAlertError("Please enter age ");
        // }
        // else if (!Util.isNumber(age) || !Util.isValidQuantity(age)) {
        //     Util.topAlertError("Please enter valid age ");
        // }
        else if (_.isEmpty(phone)) {
            Util.topAlertError("Please enter your phone number");
        }
        else if (!Util.isNumber(phone) || !(phone.length >= 10 && phone.length <= 16)) {
            Util.topAlertError("Phone Number is invalid");
        }
        else {
            const payload = {
                activity_id: navigationData.id,
                booking_date: booking_date,
                time_from: time_from.time_slot,
                no_of_jetski: no_of_jetski,
                no_of_person_per_jetski: no_of_person_per_jetski,
                full_name: full_name,
                email: email,
                countryCode: '+971',
                phone: phone,
                activity_price: activity_price,
                activity_attribute_id: activity_attribute_id,
                center_id: navigationData.center_id,
                age: 25,
                extraGuideCharges: extraGuideCharges
            };
            Util.showLoader(this);
            this.props.createActivityRequest(payload, data => {
                if (data) {
                  setTimeout(() => {
                    this.goNext("PackagesAndAccessories", data.activity_id);
                  }, 300);
                }
                Util.hideLoader(this);
            })
        }
    }

    setActivityAttribute(option) {
        this.setState({textInputValue: option.label, activity_attribute_id: option.key, activity_price: option.price, max_people: option.no_of_people, extraGuideCharges: option.extraCharges})
    }

    setPersons(num) {
        if(_.isEmpty(this.state.textInputValue)) {
            Util.topAlertError("Please select no of people");
            this.setState({no_of_person_per_jetski: ''})
            return null;
        } else {
            let activityAttributes = this.state.navigationData.get_attributes;
            activityAttributes.map(activity => {
                if(activity.id === this.state.activity_attribute_id) {
                    let input = parseInt(num);
                    let max_people = parseInt(activity.max_people)
                    let min_people = parseInt(activity.people)
                    if(input >= min_people && input <= max_people) {
                        this.setState({no_of_person_per_jetski: input.toString()})
                    } else {
                        this.setState({no_of_person_per_jetski: ''})
                    }
                }
            })
        }
    }

    setAge(age) {
        if(age >= 1 && age <=70) {
            this.setState({age:age})
        } else {
            this.setState({age:''})
        }
    }

    changeDate(bookingDate) {
        if(bookingDate < Util.getCurrentDate()) {
            Util.topAlertError("You can not select the past date");
        } else {
            Util.showLoader(this);
            let _dateObj = {
                'activity_id': this.state.navigationData.id,
                'booking_date': bookingDate
            }
            this.props.bookingDateRequest(_dateObj, data => {
                if (data) {
                    const time_from = [];
                    const timings = data.data;
                    if(timings.length>0){
                        for(var i=0;i<timings.length; i++){
                            time = { key: i, label: timings[i].time_slots, totalAssets: timings[i].number_of_assets }
                            time_from.push(time);
                        }
                    }else{
                        time = {key:0,label:"No values found"};
                        time_from.push(time);
                    }
                    this.setState({booking_date: bookingDate, timeSlotsAvailable: time_from})
                } else {
                    Util.topAlertError("Time slots are full on selected date");
                }
                Util.hideLoader(this);
            })
        }
    }

    setTimeSlot(timeObject) {
        let obj = {
            'time_slot': timeObject.label,
            'available_assets': timeObject.totalAssets
        };
        this.setState({time_from:obj})
    }

    setSkiQuantity(value) {
        if(this.state.activity_attribute_id && !_.isEmpty(this.state.activity_attribute_id)) {
            this.setState({no_of_jetski: value.label.toString(), no_of_person_per_jetski: (value.label*this.state.max_people).toString()})
        }else {
            Util.topAlertError("Select ride duration");
        }
    }

    goNext = (page, id) => {
        this.props.navigation.navigate(page,{"activity_id": id});
    };

    render(){

        const { navigation ,user} = this.props;
        const data = navigation.getParam('item');
        var attributes = (data.get_attributes);
        const peoples = [];
        const no_of_ski = [];
        const centers = [];
        if(attributes.length>0){
            for(var i=0;i<attributes.length; i++){
                // attribute = { key: attributes[i].id, label: attributes[i].duration+" mins, AED "+attributes[i].price+" (min "+attributes[i].people+" - up to "+attributes[i].max_people+" persons), "+attributes[i].title, price: attributes[i].price, no_of_people: attributes[i].max_people }
                let extraChecksAttribute = '';
                if( attributes[i].checks && attributes[i].checks.length > 0 ) {
                    for( let x = 0; x < attributes[i].checks.length; x++ ) {
                        extraChecksAttribute = ` + AED ${attributes[i].checks[x].amount} ${attributes[i].checks[x].name}`;
                    }
                }
                attribute = { key: attributes[i].id, label: attributes[i].duration+" mins, AED "+attributes[i].price+" (min "+attributes[i].people+" - up to "+attributes[i].max_people+" persons), " +attributes[i].title + extraChecksAttribute, price: attributes[i].price, no_of_people: attributes[i].max_people, extraCharges: extraChecksAttribute }
                peoples.push(attribute);
            }
        }else{
            attribute = {key:0,label:"No values found"};
            peoples.push(attribute);
        }
        if( data.centers.length > 0 ) {
            data.centers.map((item) => {
                centers.push({key: item.id, label: item.center_name})
            })
        }
        if( this.state.time_from.available_assets && this.state.time_from.available_assets > 0 ) {
            for(let i=1; i<=this.state.time_from.available_assets; i++) {
                attribute = { key: i, label: i }
                no_of_ski.push(attribute);
            }
        }else {
            no_of_ski.push({key:0,label:`No ${data.activity_name} available`})
        }

        return(
            <ScrollView
            style={[styles.container],{ backgroundColor: '#fff' }}
            contentContainerStyle={{flexGrow: 1}}
            keyboardShouldPersistTaps="handled">
                <ImageBackground source={require('../../assets/images/detail.png')} style={styles.play}>
                    <View style={styles.detailTop}></View>

                    <ImageBackground source={require('../../assets/images/order_form_background.png')} style={styles.detailMiddle} imageStyle={{ borderRadius: 8}}>
                        <View style={{ flexDirection:'row',paddingTop:10,paddingLeft:10}}>
                            <Text style={styles.topHeading}>{data.activity_name}</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <View style={styles.formGroup}>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Ride</Text>
                                    <ModalSelector
                                        data={peoples}
                                        initValue="Ride Mins"
                                        supportedOrientations={['landscape']}
                                        accessible={true}
                                        overlayStyle={{ flex: 1, padding: '5%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}
                                        style={styles.formInputDetail2}
                                        selectStyle={styles.selectStyle}
                                        selectTextStyle={{textAlign: 'left'}}
                                        initValueTextStyle={{textAlign: 'left', color: '#aeaeae'}}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option)=> this.setActivityAttribute(option)}>
                                        {/* <TextInput style={{borderColor: 'red', borderWidth: 1}} placeholder={`Ride Mins${this.state.inputState == 'PORTRAIT' ? space130: space120}`} value={this.state.textInputValue} ></TextInput> */}
                                    </ModalSelector>
                                </View>
                            </View>

                            <View style={styles.formGroup}>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Center</Text>
                                    <ModalSelector
                                        data={centers}
                                        initValue="Center"
                                        initValueTextStyle={{textAlign: 'left', color: '#aeaeae'}}
                                        supportedOrientations={['landscape']}
                                        accessible={true}
                                        overlayStyle={{ flex: 1, padding: '5%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}
                                        style={styles.formInputDetail}
                                        selectStyle={styles.selectStyleHalf}
                                        selectTextStyle={{textAlign: 'left'}}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option)=> this.setState({center_id: option.key, center_name: option.label})}>
                                        {/* <TextInput placeholder={`No of jetski${this.state.inputState == 'PORTRAIT' ? space90: space120}`} value={this.state.no_of_jetski}></TextInput> */}
                                    </ModalSelector>
                                </View>

                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Date of booking</Text> 
                                    <View style={styles.formInputDetail}>
                                        <DatePicker
                                            style={styles.input2DatePicker}
                                            date={this.state.booking_date}
                                            mode="date"
                                            placeholder="YYYY/MM/DD"
                                            format="YYYY-MM-DD"
                                            minDate="2019-05-01"
                                            maxDate="2020-12-31"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            showIcon={false}
                                            customStyles={{
                                            dateIcon: {
                                                position: 'absolute',
                                                left: 0,
                                                top: 4,
                                                marginLeft: 0
                                            },
                                            dateInput: {
                                                //marginLeft: 10,
                                                fontSize:6,
                                                borderColor:'#fff',
                                                alignItems:'flex-start' 
                                            }
                                        }}
                                        onDateChange={(booking_date) => this.changeDate(booking_date)} />
                                    </View>
                                </View>
                            </View>

                            <View style={styles.formGroup}>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Select Time Slot</Text>
                                    <ModalSelector
                                        data={this.state.timeSlotsAvailable}
                                        initValue="Time Slot"
                                        initValueTextStyle={{textAlign: 'left', color: '#aeaeae'}}
                                        supportedOrientations={['landscape']}
                                        accessible={true}
                                        overlayStyle={{ flex: 1, padding: '5%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}
                                        style={styles.formInputDetail}
                                        selectStyle={styles.selectStyleHalf}
                                        selectTextStyle={{textAlign: 'left'}}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option)=> this.setTimeSlot(option)}>
                                        {/* <TextInput placeholder={`Time Slot${this.state.inputState == 'PORTRAIT' ? space90: space120}`} value={this.state.time_from.time_slot} ></TextInput> */}
                                    </ModalSelector>
                                </View>

                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Select No. of {this.state.navigationData ? this.state.navigationData.activity_name:''}</Text>
                                    <ModalSelector
                                        data={no_of_ski}
                                        initValue={`No of ${this.state.navigationData ? this.state.navigationData.activity_name:''}`}
                                        initValueTextStyle={{textAlign: 'left', color: '#aeaeae'}}
                                        supportedOrientations={['landscape']}
                                        accessible={true}
                                        overlayStyle={{ flex: 1, padding: '5%', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0.8)' }}
                                        style={styles.formInputDetail}
                                        selectStyle={styles.selectStyleHalf}
                                        selectTextStyle={{textAlign: 'left'}}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option)=> this.setSkiQuantity(option)}>
                                        {/* <TextInput placeholder={`No of jetski${this.state.inputState == 'PORTRAIT' ? space90: space120}`} value={this.state.no_of_jetski}></TextInput> */}
                                    </ModalSelector>
                                </View>
                                {/* <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Number of persons</Text>
                                    <View style={styles.formInputDetail}>
                                        <TextInput style={styles.input2} editable={false} value={this.state.no_of_person_per_jetski} placeholder='Persons Per Ride'></TextInput>
                                    </View>
                                </View> */}
                            </View>

                            <View style={styles.formGroup}>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Number of persons</Text>
                                    <View style={styles.formInputDetail}>
                                        <TextInput style={styles.input2} editable={false} value={this.state.no_of_person_per_jetski} placeholder='Persons Per Ride'></TextInput>
                                    </View>
                                </View>

                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Full Name</Text> 
                                    <View style={styles.formInputDetail}>
                                        <TextInput style={styles.input2} returnKeyType="next" ref={(input)=>this.full_name = input}  selectionColor='#000' placeholder='Full Name'   onChangeText={full_name => this.setState({full_name})}></TextInput>
                                    </View>
                                </View>
                                {/* <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Email</Text> 
                                    <View style={styles.formInputDetail}>
                                        <TextInput style={styles.input2} selectionColor='#000' returnKeyType="next" placeholder='Email' onChangeText={email => this.setState({email})}></TextInput>
                                    </View>
                                </View> */}
                            </View>

                            <View style={styles.formGroup}>
                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Email</Text> 
                                    <View style={styles.formInputDetail}>
                                        <TextInput style={styles.input2} selectionColor='#000' returnKeyType="next" placeholder='Email' onChangeText={email => this.setState({email})}></TextInput>
                                    </View>
                                </View>

                                <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Phone Number</Text>
                                    <View style={styles.formInputDetail3}>
                                        <CountryPicker
                                            onSelect={(value)=> this.setState({ flag: value.cca2, callingCode: value.callingCode[0] }) }
                                            countryCode={this.state.flag}
                                        />
                                        <Text style={{ color: '#A2A2A2'}}>{this.state.callingCode ? `+${this.state.callingCode}`:null}</Text>
                                        <View style={{marginLeft: 10, marginRight: 10, width: StyleSheet.hairlineWidth, backgroundColor: '#000', height: 20}} />
                                        <TextInput style={styles.input2} keyboardType={'numeric'} ref={(input)=>this.phone = input} selectionColor='#000' placeholder="Phone Number" onChangeText={phone => this.setState({phone})}></TextInput>
                                    </View>
                                </View>
                                {/* <View style={styles.inputContainer}>
                                    <Text style={styles.labelTop}>Age</Text>
                                    <View style={styles.formInputDetail}>
                                        <TextInput style={styles.input2} ref={(input)=>this.age = input} keyboardType={'numeric'} returnKeyType="next"  selectionColor='#000' placeholder='Age' value={this.state.age} onChangeText={age => this.setAge(age)}></TextInput>
                                    </View>
                                </View> */}
                            </View>
                        </View>
                    </ImageBackground>

                    <View style={styles.detailBottom}>
                        <ButtonView activeOpacity={0.5} style={styles.buttonContainer} onPress={   this.submitForm  }>
                            <Image source={require('../../assets/images/button.png')} style={{width:'100%',height:this.state.inputState == 'PORTRAIT' ? 60 : 80}} />  
                            <Text style={styles.loginButtonText}>NEXT</Text> 
                        </ButtonView>
                    </View>
                    <View style={styles.detailFooter}></View>
                </ImageBackground>
                <Loader loading={this.state.loading} />
            </ScrollView>
        );
    }
}
const mapStateToProps = ({user, activities }) => {
    return({
        user:user.data,
        selectedActivity: activities.selectedActivity
    });
}
const actions = {userSignOutRequest, createActivityRequest, bookingDateRequest };

export default connect(
  mapStateToProps,
  actions
)(ActivityDetailPageComponent);

