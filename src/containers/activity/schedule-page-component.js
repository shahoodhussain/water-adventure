/**
 * Created by faraz ali on 13/05/2019.
 */

import React from 'react';
import { View, Text,FlatList,TouchableOpacity,Alert } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import { getAllClassesRequest } from "../../actions/ClassesAction";
import { connect } from "react-redux";
import _ from "lodash";
import Util from "../../util";
import {  Loader,EmptyStateText } from "../../components";
import{CLASS_LIST,TIME_FORMAT2,weekdays} from "../../constants";


class SchedulePageComponent extends React.Component {
    state={
        loading: false
    }
    componentDidMount() {
      this._getClasses()
      //  MessageBarManager.registerMessageBar(this.refs.alert);
    }

    _getClasses=()=>{
        Util.showLoader(this);
        this.props.getAllClassesRequest((data)=>{
        if(data){
            Util.hideLoader(this);
        }
    });
    }

    goNext=(page,item)=>{
        //Alert.alert(page);
        this.props.navigation.navigate(page)
    };



    renderItems=({item})=> {
     let time=   Util.getFormattedDateTime(item.start_class_time,TIME_FORMAT2);
     const dateLength=item.start_class_time.length; 
     const date=item.start_class_time
     let onlyDate = date.substring(8, dateLength - 8);
     const dayName=weekdays[Util.getDay(date)]

     return(
                <View  style={styles.Scheduleflatview}>
                <View style={styles.scheduleLeftDateView}>
                    <Text>{dayName}</Text>
                    <Text style={{color:'#f4c217',fontSize:20,fontWeight:'bold'}}>{onlyDate}</Text>
                </View>
                <View style={styles.scheduleLeftSeperaterView}>
                       <Text></Text>
                </View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('ClassDetail',{item:item})
                } style={styles.textView}>

                    <Text style={styles.notification3}>{item.class_type}</Text>
                    <Text
                    numberOfLines={2}
                    style={styles.time2}>{item.location}</Text>


                </TouchableOpacity>
                <View style={styles.scheduleRightTimeView}>
                    <Text style={{color:'#f4c217',fontWeight:'bold'}}>{time}</Text>
                </View>

                </View>


        )
    };

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: "#d5e1e8",
                }}
            />
        );
    };
    _renderEmptyComponent =  (isLoading)=> {
        if (isLoading) {
          return null;
        }
        return (
         <EmptyStateText/> 
        );
      };
    
    render() {
        const { loading } = this.state;
        const {classes,Userdata}=this.props

        return (
            <View style={styles.navigationContainer}>
                <View style={styles.topView}>
                    <Text style={styles.scheduleHeading}>Schedule Training Dates</Text>
                </View>

                <View style={styles.notificationView}>



                    <FlatList
                        data={classes}
                        contentContainerStyle={{ paddingBottom:20}}
                        renderItem={this.renderItems}
                        keyExtractor={item => item.key}
                        ListEmptyComponent={()=>this._renderEmptyComponent(loading)}
                        refreshing={loading}
                        showsVerticalScrollIndicator={false}
                        onRefresh={this._getClasses}

                    />



                </View>
                {Userdata.utype==="ctrainer"?
                <View style={styles.scheduleLowerView}>
                    <TouchableOpacity  activeOpacity={0.7} onPress={()=>this.props.navigation.navigate('ScheduleForm')} style={styles.profileButton}>
                        <Text style={{fontSize:18,color:'#000'}}>Schedule a Class</Text>
                    </TouchableOpacity>
                </View>:null}
              {/*  <Loader loading={this.state.loading} />*/}

            </View>
        );
    }


}


const mapStateToProps = ({classes, user}) => ({
    classes:classes.data,
    Userdata: user.data
});
const actions = { getAllClassesRequest };

export default connect(
  mapStateToProps,
  actions
)(SchedulePageComponent);