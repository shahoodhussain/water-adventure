import React from 'react'
import { View, Text, Image, Dimensions } from 'react-native';
import { ButtonView } from "../../../components";
import styles from '../../../assets/stylesheets/global-styles';
const imageStyle = Dimensions.get('window').width < Dimensions.get('window').height ? {width:'100%',height:60} : {width:'80%',height:70};

export const InvoicePayment = (props) => {
    let {sub_total, grand_total, coupon, tax, discount} = props.total ? props.total : {}
    tax = (sub_total * 0.05);
    sub_total = sub_total - tax;
    return (
        <View style={styles.paymentDetails}>
            <View style={styles.amountDetails}>
                <Text style={styles.amountText}>Total:</Text>
                <Text style={styles.amountText}>AED {sub_total}</Text>
            </View>
            {/* <View style={styles.amountDetails}>
                <Text style={styles.amountText}>Delivery Charge:</Text>
                <Text style={styles.amountText}>AED 0</Text>
            </View> */}
            <View style={styles.amountDetails}>
                <Text style={styles.amountText}>Discount:</Text>
                <Text style={styles.amountText}>AED {discount && discount != null ? discount.toFixed(2):'0'}</Text>
            </View>
            <View style={styles.amountDetails}>
                <Text style={styles.amountText}>VAT 5%:</Text>
                <Text style={styles.amountText}>AED {tax && tax != null ? tax.toFixed(2): '0'}</Text>
            </View>
            <View style={styles.lastRow}>
                <Text style={styles.amountText}>Grand Total:</Text>
                <Text style={styles.amountText}>AED {grand_total}</Text>
            </View>
        </View>
    )
}

export const SubmitButton = (props) => {
    return (
        <View style={styles.bookNowBox}>
            <ButtonView activeOpacity={0.5} style={styles.bookNowButton} onPress={props.submit}>
                <Image source={require('../../../assets/images/button.png')} style={imageStyle} />
                <Text style={styles.bookNowButtonText}>{props.title}</Text>
            </ButtonView>
        </View>
    
    )
}