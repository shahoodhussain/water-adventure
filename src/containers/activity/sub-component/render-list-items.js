import React from 'react'
import { View, Text, Image as RNImage, TouchableOpacity } from 'react-native';
import styles from '../../../assets/stylesheets/global-styles';
import Icon from "react-native-vector-icons/FontAwesome5";

const imageURL = 'http://dev70.onlinetestingserver.com/water-adventure-new/public/uploads/';

export const InvoiceItem = (props) => {
    let { currentState, currentItem, handleQuantityChange, removeItem } = props;
    let { total, title, image, id, quantity } = currentItem;
    let quantityView = '';
    if( currentItem.item_type == "accessory" ) {
        quantityView = (
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={()=>handleQuantityChange(id, "REMOVE")}>
                    <Icon name="minus-circle" size={18} color="#f06d12"/>
                </TouchableOpacity>
                <Text style={{width: 30, textAlign: 'center', fontSize: 16, color: '#000'}}>{quantity ? quantity : 1}</Text>
                <TouchableOpacity onPress={()=>handleQuantityChange(id, "ADD")}>
                    <Icon name="plus-circle" size={18} color="#f06d12"/>
                </TouchableOpacity>
            </View>
        )
    } else {
        quantityView = (
            <View style={{alignItems: 'center'}}>
                <Text style={{width: 20, fontSize: 16, color: '#000'}}>{quantity ? quantity : 1}</Text>
            </View>
        )
    }
    return (
        <View style={styles.invoiceItem} key={id}>
            <View style={styles.invoiceItemFirstBox}>
                <View style={currentState ? styles.invoiceItemImagePortrait:styles.invoiceItemImageLandscape}>
                    <RNImage style={{flex: 1}} source={{uri: imageURL+image}}/>
                </View>
                <Text style={currentState ? styles.invoiceItemTitlePortrait:styles.invoiceItemTitleLandscape} textBreakStrategy="balanced">{title?title:'Title not specified'}</Text>
            </View>
            <View style={styles.invoiceItemSecondBox}>
                <View style={currentState?styles.itemDetailsPortrait:styles.itemDetailsLandscape}>
                    <Text style={{paddingBottom: 15}}>Quantity</Text>
                    {/* <Text style={{color: '#288CCE', fontSize: 16}}>{quantity} item</Text> */}
                    {quantityView}
                </View>
                <View style={currentState?styles.itemDetailsPortrait:styles.itemDetailsLandscape}>
                    <Text style={{paddingBottom:15}}>Price</Text>
                    <Text style={{color: '#288CCE', fontSize: 16}}>AED {total}</Text>
                </View>
                {
                    currentItem && typeof currentItem != "undefined" && currentItem.item_type && currentItem.item_type != "activity" && (
                        <TouchableOpacity style={currentState?styles.itemDetailsPortrait:styles.itemDetailsLandscape} onPress={()=>removeItem(id, currentItem.item_type)}>
                            <Icon name="trash-alt" size={24} color="#f06d12"/>
                        </TouchableOpacity>
                    )
                }
            </View>
        </View>
    )
}