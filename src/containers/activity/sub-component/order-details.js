import React from 'react'
import { View, Text } from 'react-native';
import styles from '../../../assets/stylesheets/styles';
import Icon from "react-native-vector-icons/FontAwesome5";

export const ActivityOrderDetails = (props) => {
    let {booking_object} = props;
    console.log('props asdasd',props)
    return (
        <View style={styles.orderDetailsBox}>
            <View style={{marginTop: 10, marginLeft: 10}}>
                <Text style={styles.sectionHeading}>ORDER DETAIL</Text>
            </View>
            <View style={styles.orderDetailRow}>
                <Icon name="clipboard-list" size={26} color="#f06d12"/>
                <View style={styles.orderInfoBox}>
                    <Text style={{fontSize: 18}}>Order#</Text>
                    <Text style={styles.orderValueText}>{booking_object ? booking_object.order_no : '0'}</Text>
                </View>
            </View>
            <View style={styles.orderDetailRow}>
                <View style={styles.innerRow}>
                    <Icon name="clock" size={26} color="#f06d12"/>
                    <View style={styles.orderInfoBox}>
                        <Text style={{fontSize: 20}}>Order Time</Text>
                        <Text style={styles.orderValueText}>{booking_object ? booking_object.order_time:'-'}</Text>
                    </View>
                </View>
                <View style={[styles.innerRow, {marginLeft: 30}]}>
                    <Icon name="calendar-alt" size={26} color="#f06d12"/>
                    <View style={styles.orderInfoBox}>
                        <Text style={{fontSize: 20}}>Order Date</Text>
                        <Text style={styles.orderValueText}>{booking_object ? booking_object.order_date:'-'}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.orderDetailRow}>
                <View style={styles.innerRow}>
                    <Icon name="map-marker-alt" size={26} color="#f06d12"/>
                    <View style={styles.orderInfoBox}>
                        <Text style={{fontSize: 20}}>Center Name</Text>
                        <Text style={styles.orderValueText}>{booking_object ? booking_object.center_name : 'not specfied'}</Text>
                    </View>
                </View>
                <View style={[styles.innerRow, {marginLeft: 30}]}>
                    <Icon name="calendar-alt" size={26} color="#f06d12"/>
                    <View style={styles.orderInfoBox}>
                        <Text style={{fontSize: 20}}>Activity Time</Text>
                        <Text style={styles.orderValueText}>{booking_object ? booking_object.time_slot : 'not specfied'}</Text>
                    </View>
                </View>
            </View>
            {props.children}
        </View>
    )
}

export const TotalSection = (props) => {
    let booking_details = props.booking_details;
    let tax = (booking_details.sub_total * 0.05);
    let sub_total = booking_details.sub_total - tax;
    return(
        <View>
            <View style={styles.receiptRowTotal}>
                <Text style={styles.receiptText}>Total:</Text>
                <Text style={styles.receiptText}>AED {sub_total}</Text>
            </View>
            <View style={styles.receiptRowTotal}>
                <Text style={styles.receiptText}>Discount:</Text>
                <Text style={styles.receiptText}>AED {booking_details.discount}</Text>
            </View>
            <View style={styles.receiptRowTotal}>
                <Text style={styles.receiptText}>VAT 5%:</Text>
                <Text style={styles.receiptText}>AED {tax && tax != null ? tax.toFixed(2): '0'}</Text>
            </View>
            <View style={styles.receiptRowTotal}>
                <Text style={styles.receiptText}>Grand Total:</Text>
                <Text style={styles.receiptText}>AED {booking_details.grand_total}</Text>
            </View>
        </View>
    )
}