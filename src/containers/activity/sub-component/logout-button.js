import React from 'react'
import { Image as RNImage } from 'react-native';
import { ButtonView } from "../../../components";
import styles from '../../../assets/stylesheets/global-styles';


export const LogoutHeaderButton = (props) => {
    return(
        <ButtonView activeOpacity={0.7} onPress={props.logout}   style={styles.logoutContainer}>
            <RNImage source={require('../../../assets/images/logout_icon.png')} style={styles.play} />  
        </ButtonView>
    )
}