/**
 * Created by faraz ali on 13/05/2019.
 */

import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  TextInput,
  Image,
  ScrollView,
  Alert,
  Button,
  SafeAreaView,
  ImageBackground,
  
} from "react-native";
import styles from "../../assets/stylesheets/styles.js";
import DatePicker from "react-native-datepicker";
import Modal from "react-native-modal";
import ImagePicker from "react-native-image-picker";
//import SectionedMultiSelect from "react-native-sectioned-multi-select";
import {
  Dimensions,
  Platform,
  StyleSheet,
  ActivityIndicator
} from "react-native";
const { width, height } = Dimensions.get("window");
const screenWidth = width; // < height ? width : height;
const screenHeight = height; // < height ? height : width;
import Icon from "react-native-vector-icons/dist/Ionicons";
import * as Progress from "react-native-progress";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from "react-redux";
import Util from "../../util";
import { getClassTypesRequest,getAllClassesRequest,createCardRequest} from "../../actions/ClassesAction";
import _ from "lodash";
import { Loader } from "../../components/index.js";
import { NavigationEvents } from "react-navigation";


class CreateCardComponent extends React.Component {
  constructor(props) {
    super(props);
    const {classesTypes,classesDates}=this.props;
    this.state = {
       images:[],

      documentsLoading:false,
      selectedClassType:"",
      profileImage:"",
      date: null,
      modalVisible: false,
      avatarSource: null,
      selectedDates: [],
      progress: null,
      imageSource: [],
      loading: false,
      name:"",
      randomNumber:Math.random().toString(36).slice(2),
      classType:[
        {
          name: "Lift Type",
          id: 0,
          // these are the children or 'sub items'
          children:classesTypes
        }
      ],
      classDates:[
        {
          name: "Date Of Class",
          id: 0,
          children:classesDates

        },  
        ]
  }
}
clearField=()=>{
  this.setState({
    images:[],
    selectedClassType:"",
      profileImage:"",
      date: null,
      modalVisible: false,
      avatarSource: null,
      selectedDates: [],
      progress: null,
      imageSource: [],
      loading: false,
      name:"",
      randomNumber:Math.random().toString(36).slice(2),
  })
}
 submitRequest(){
   const {name,date,selectedDates,profileImage}=this.state;
   if(_.isEmpty(profileImage)){
     Util.topAlertError("please upload profile picture")
   }
   if(_.isEmpty(name)){
     Util.topAlertError("Enter name")
   }
   else if(_.isEmpty(date)){
     Util.topAlertError("select Date")
   }
   else {
    Util.showLoader(this);
    const formData = new FormData()
    formData.append('name', name);
    formData.append('class_id', selectedDates.toString());
    formData.append('card_no', this.state.randomNumber);
    const payload={
      uri: this.state.profileImage,
      type: 'image/png',
      name: "profile_picture"
  }
      formData.append("picture",payload);
  this.state.images.map((item, i) => {
    formData.append("documents[]", {
      uri: item.uri,
      type: "image/jpeg",
      name: item.filename || `filename${i}.jpg`,
    });
  });
this.props.createCardRequest(formData,(data)=>{
  if(data){
    Util.hideLoader(this);
    Util.topAlert("card created successfully")
    this.clearField();
  }
 
})
  

   }
 }
  onClassTypeChange = selectedItems => {
    this.setState({  selectedClassType:selectedItems });
  };
  closeModal = () => {
    this.setState({ modalVisible: false });
  };
  toggleModal = () => {
    this.setState({ modalVisible: !this.state.modalVisible });
  };
  showLoader = () => {
    if (this.state.documentsLoading) {
      return (
        <ActivityIndicator
          animating
          size="small"
          style={{ alignSelf: "center" }}
        />
      );
    }
  };
  openCamera = () => {
    if(this.state.imageSource.length===2){
      Util.topAlertError("not allowed more than two documents")

    }
    else {
      this.setState({ documentsLoading: true });

      const options = {
        title: "Select Document",
        storageOptions: {
          skipBackup: true,
          path: "images"
        },
        quality:0.1,
        maxWidth:150,
        maxHeight:150

      };
  
      ImagePicker.showImagePicker(options, response => {
        console.log("Response = ", response);
  
        if (response.didCancel) {
          console.log("User cancelled image picker");
          this.setState({ documentsLoading: false });
        } else if (response.error) {
          console.log("ImagePicker Error: ", response.error);
          this.setState({ documentsLoading: false });
        } else if (response.customButton) {
          console.log("User tapped custom button: ", response.customButton);
          this.setState({ documentsLoading: false });
        } else {
         
          const source = response;
        
  
         // const documents=response.uri;
          this.state.images.push(source);
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
          this.setState({
            imageSource: this.state.imageSource.concat(source),
           // documents: ,
            progress: 1,
            documentsLoading: false
          });
        }
      });
    }
    }
    
 
    
  openProfileCamera = () => {

    const options = {
      title: "Select Document",
      storageOptions: {
        skipBackup: true,
        path: "images"
      },
      quality:0.1,
        maxWidth:150,
        maxHeight:150
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      //  this.setState({ loading: false });
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      //  this.setState({ loading: false });
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
       // this.setState({ loading: false });
      } else {
        const source = response.uri;
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          profileImage: source,
        });
      }
    });
  };
  onSelectedDateChange = selectedItems => {
    this.setState({selectedDates:selectedItems });
  };
  removeItem = index => {
    var checked = this.state.imageSource;
    var values = checked.indexOf(index);
    checked.splice(values, 1);
    this.setState({ imageSource: checked });
    console.log(this.state.imageSource);
  };
  viewFiles = () => {
    let image = this.state.imageSource;
    return this.state.imageSource.map((image, index) => {
      return (
        <View>
          <View
            style={{
              margin: 10,
              // flex: 1,
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row"
              // backgroundColor: "red"
            }}
          >
            <Image
              source={require("../../assets/images/file_icon.png")}
              style={{ height: 50, width: 50 }}
              resizeMode="contain"
            />
            <View>
              <Text numberOfLines={1} style={{ width: 100 }}>
                {image.fileName}
              </Text>
              <Progress.Bar progress={1} width={200} color="#f4c217" />
            </View>
            <TouchableOpacity onPress={() => this.removeItem(index)}>
              <Icon
                name="md-close"
                style={{ marginHorizontal: 10, fontSize: 20 }}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };
  onSelectedDateObjectChange=(data)=>{
this.setState({date:data[0].name})
  }
  cardPreview=()=>{
    const {data}=this.props;
    return(
      <Modal
      animationType="fade"
      backdropOpacity={0.6}
      hasBackdrop={true}
      backdropColor="black"
      isVisible={this.state.modalVisible}
      
    >
    
      <View
        onPress={this.toggleModal}
        style={{
          justifyContent: "center",
          alignItems: "center",
          transform: [{ rotate: "-90deg" }],
              height: screenHeight
        }}
      >
       
        <TouchableOpacity
          style={{
            backgroundColor: "#fff",
           
            justifyContent: "space-between",
            alignItems: "flex-end",
            borderBottomWidth: 1,
            flexDirection: "row"
          }}
          activeOpacity={0.8}
          onPress={this.toggleModal}
        >
          <Image
            source={require("../../assets/images/edit.png")}
            style={{ height: 20, width: 20 }}
          />
    
        </TouchableOpacity>
        <View
          style={{
            width: screenHeight - 150,
           
            backgroundColor: "#fff",
            flexDirection: "row",
            justifyContent: "space-between",
            overflow: "visible"
          }}
        >
          <View style={{ flex: 7, marginLeft: 10 }}>
            <Text
              style={{
              
                fontWeight: "bold",
                marginVertical: 5,
                fontSize: 25
              }}
            >
              Fork lift University
            </Text>
            <Text
              style={{
                marginVertical: 5,
                fontSize: 15
              }}
            >
              www.forkliftuniversity.com +917-732-1695
            </Text>
            <Text
              style={{
                marginVertical: 5,
              
                fontSize: 15
              }}
            >
              Authorize Instructor
            </Text>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              margin: 5,
              flex: 3
            }}
          >
            <Image
             source={require("../../assets/images/cardLogo.png")}
           
             style={{
             
                resizeMode: "contain",
                height: 100,
                width: 100
              
              }}
            />
          </View>
        </View>
    
        <View
          style={{
            borderBottomWidth: 1
           
          }}
        />
        <View
          style={{
            width: screenHeight - 150,
            backgroundColor: "#fff",
            flexDirection: "row",
            justifyContent: "space-between",
            overflow: "visible"
          }}
        >
          <View style={{ flex: 7 }}>
            <View style={{ margin: 5, flexDirection: "row" }}>
              <Text
                style={{
                  color: "#000",
                  fontWeight: "bold",
                  fontSize: 20
                }}
              >
               Name:
              </Text>
              <Text
                style={{
                  color: "gray",
                  fontWeight: "bold",
                  fontSize: 20
                }}
              >
              {this.state.name}
              </Text>
            </View>
            <View style={{ margin: 5, flexDirection: "row" }}>
              <Text
                style={{
                  color: "#000",
                  fontWeight: "bold",
                  fontSize: 20
                }}
              >
                ID Number:
              </Text>
              <Text
                style={{
                  color: "gray",
                  fontWeight: "bold",
                  fontSize: 20
                }}
              >
                {this.state.randomNumber}
              </Text>
            </View>
            <View style={{ margin: 5, flexDirection: "row" }}>
              <Text
                style={{
                  color: "#000",
                  fontWeight: "bold",
                  fontSize: 20
                }}
              >
                Date of Class:
              </Text>
              <Text
                style={{
                  color: "gray",
                  fontWeight: "bold",
                  fontSize: 20
                }}
              >
              {this.state.date}
              </Text>
            </View>
            <View style={{ margin: 5, flexDirection: "row" }}>
              <Text style={{ color: "#000", fontSize: 20 }}>
                Authorized Instructor:
              </Text>
              <Text style={{ color: "#000", fontSize: 20 }}>
                  {data.name}
             </Text>
            </View>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              margin: 5,
              flex: 3
            }}
          >
            <Image
             source={{uri:this.state.profileImage}}

             style={{
                resizeMode: "contain",
                height: 130,
                width: 130
              }}
            />
          </View>
        </View>
        <View
          style={{
            backgroundColor: "red",
            borderBottomWidth: 1,
            borderColor: "red"
          }}
        />
        <View
          style={{
            width: screenHeight - 150,
            backgroundColor: "#fff",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              textAlign: "center",
              marginVertical: 5,
              color: "gray",
              fontSize: 15
            }}
          >
            of randomized words which don't look even slightly believable.
            if you going to use a passage of lorem ipsum,you need to be sure
            there isn't anything embarrassing
          </Text>
        </View>
      </View>
    
    </Modal>
    )
  }
  _getData=()=>{
    Util.showLoader(this);
    this.props.getAllClassesRequest((data)=>{
      console.log(data,"datacheck")
      this.DateTypes=[
        {
          name: "Date of Class",
          id: 0,
          children:data
        }
      ]
      Util.hideLoader(this);  

    }); 
  }

  DateTypes=[
    {
      name: "Class Type",
      id: 0,
      children:[]
    }
  ]

  render() {
    console.log(this.state.imageSource,"images")
    let imagesource=this.state.profileImage? {uri:this.state.profileImage}:require("../../assets/images/camera.png")
    return (
      <SafeAreaView style={styles.navigationContainer}>
      <NavigationEvents
                    
                    onDidFocus={payload => {
                      this._getData()
                                         }}
                />
      <KeyboardAwareScrollView
      enableOnAndroid
      keyboardShouldPersistTaps={"handled"}
     style={{backgroundColor:"#fff" }}
     showsVerticalScrollIndicator={false}>
        <ScrollView
          contentContainerStyle={{ paddingBottom: 20 }}
          showsVerticalScrollIndicator={false}
        >
      
          <View style={styles.classTopView}>
            <Text style={styles.scheduleHeading}>Create A Card</Text>
                                       
           <TouchableOpacity
              activeOpacity={0.5}
              
              onPress={
              
                () => this.props.navigation.navigate("Search")
              }
              style={{ height: 40, padding: 5, marginTop: 40 }}
            >
              <Image
                style={[styles.play]}
                source={require("../../assets/images/search.png")}
              />
            </TouchableOpacity>

          </View>
          <View style={styles.cardMiddleView}>
            <TouchableOpacity
              onPress={() => 
               this.openProfileCamera()}
              style={styles.imageView}
            >
              <Image
                style={this.state.profileImage? {height: 100, width: 100 ,borderRadius:50}:{height: 100, width: 100 }}
               source={imagesource}
              />
              <Text style={{ fontSize: 13, color: "#aeaeae" }}>
                Tap to take student picture
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.classFormFieldsView}>
            <View style={styles.cardInput}>
              <TextInput
                style={styles.profileInputField}
                selectionColor="#900816"
                placeholder="Student Name"
                onChangeText={name => this.setState({ name })}
                value={this.state.name}
              />
            </View>
          </View>

      

              <View
              style={{
                flex: 1,
                marginLeft:20,
                justifyContent: "center",
              }}
            >
              
             {/* <SectionedMultiSelect
                single
                items={this.DateTypes} 
                uniqueKey="id"
                subKey="children"
                selectText="Date Of Class"
                showDropDowns={true}
                expandDropDowns={true}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedDateChange}
               onSelectedItemObjectsChange={(data)=>
                this.onSelectedDateObjectChange(data)
               }
                selectedItems={this.state.selectedDates}
                hideSearch
                alwaysShowSelectText
                submitButtonColor="#f4c217"
                
              /> */}
            </View>
           
          <View style={{ 
          
            backgroundColor:"#000",
            height:0.4,
           marginHorizontal:30
           }}/>
          <Text style={{ margin: 20 ,marginLeft:30}}>Documents Required</Text>
          <TouchableOpacity onPress={() => this.openCamera()}>
            <ImageBackground
              
              source={require("../../assets/images/box.png")}
              style={{
                alignSelf: "center",
               
                height: 100,
                width: screenWidth - 100,
               
                justifyContent: "center",
                alignItems: "center",
               
                resizeMode: "cover"
              }}
            >
              <Text style={{ fontSize: 20 }}>Upload Documents Here</Text>

              <Text style={{ fontSize: 10, textAlign: "center" }}>
                Please Attach images of Domicile and other Documents
              </Text>
            </ImageBackground>
            {this.showLoader()}
          </TouchableOpacity>
          {this.viewFiles()}

          <View style={styles.cardFormLowerView}>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                this.setState({ modalVisible: !this.state.modalVisible })
              }
              style={styles.cardButton}
            >
              <Text style={{ fontSize: 17, color: "#000" }}>Preview</Text>
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() =>
                Alert.alert(
                  "Forklift",
                  "Would you like to process card?",
                  [{ text: "Yes",
                onPress:()=>{
                  this.submitRequest();
                }
                }, { text: "No" }]
                )
              }
              style={styles.cardButton}
            >
              <Text style={{ fontSize: 17, color: "#000" }}>Process Now</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        </KeyboardAwareScrollView>


       {this.cardPreview()}
       <Loader loading={this.state.loading} />
       </SafeAreaView>
    );
  }
}
const mapStateToProps = ({ user,classes}) => ({ data: user.data,
classesTypes:classes.classTypes,
classesDates:classes.data

});

const actions = {getClassTypesRequest,getAllClassesRequest,createCardRequest};

export default connect(
  mapStateToProps,
  actions
)(CreateCardComponent);

