import React from 'react';
import { View, Text,TouchableOpacity,Image,TextInput } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import { CustomPicker } from 'react-native-custom-picker'



class SearchDetailComponent extends React.Component {

    state={
        yearValue:false
    };

    render(){
        const options3 = ['1 Year','2 Years','3 Years','4 Years','5 Years'];
        return(
            <View style={styles.navigationContainer}>
                <View style={styles.searchTopView}>
                    <Text style={styles.scheduleHeading}>Renew A Card</Text>
                </View>
                <View style={styles.dropDownView}>
                    <View style={{ width:'94%', justifyContent: 'center' }}>
                        <CustomPicker
                            options={options3}
                            modalAnimationType="fade"
                            value={this.state.yearValue}
                            placeholder={'Extend Year'}
                            onValueChange={value => {
                            this.setState({'yearValue':value});
                        }
                        }

                        /></View>
                    <View style={{height:14,width:5,marginRight:'6%'}}>

                        <Image source={require('../../assets/images/dropdown.png')} style={styles.play} />

                    </View>

                </View>
                <View style={styles.scheduleFormLowerView}>
                    <TouchableOpacity  activeOpacity={0.7} onPress={()=>this.props.navigation.navigate('Home')} style={styles.profileButton}>
                        <Text style={{fontSize:18,color:'#000'}}>Done</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:4}}></View>
            </View>

        );
    }
}

export default SearchDetailComponent;