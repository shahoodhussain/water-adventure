import React from 'react';
import { View, Text,TouchableOpacity,Image,Alert } from 'react-native';
import styles from '../../assets/stylesheets/styles.js';


class CardEnlargeComponent extends React.Component {
    render(){
        return(
            <View style={styles.enlargeContainer}>
                <View style={styles.imageEnlargeView}>
                    <Image style={[styles.play]} source={require('../../assets/images/user_full.png')} />
                </View>
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Card')} style={{backgroundColor:'#fff',borderRadius:35,padding:20,height:70,width:70,justifyContent:'center',alignItems:'center'}}>
                        <Image style={[styles.play]} source={require('../../assets/images/check.png')} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default CardEnlargeComponent;