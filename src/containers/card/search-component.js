import React from 'react';
import { View, Text,TouchableOpacity,Image,TextInput,FlatList ,ActivityIndicator} from 'react-native';
import styles from '../../assets/stylesheets/styles.js';
import { connect } from "react-redux";

class searchComponent extends React.Component {
 state={
    isSearching:false
}
    renderItems=({item})=> {
        return(
            <TouchableOpacity activeOpacity={0.8} onPress={()=>this.props.navigation.navigate('SearchDetail')} style={styles.flatview2}>

                <View style={styles.scheduleLeftDateView}>
                    <View style={{justifyContent:'center',alignItems:'center',padding:2}}>
                        <Image source={require('../../assets/images/image01.png')} style={styles.play} />
                    </View>
                </View>
                <View style={styles.textView}>

                    <Text style={styles.notification3}>{item.name}</Text>
                    <Text style={styles.time}>{item.subtitle}</Text>

                </View>
            </TouchableOpacity>
        )
    };
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    backgroundColor: "#d5e1e8",
                }}
            />
        );
    };
_getSearchRequest(query = "") {
    const payload = {
          query
           };
        Util.showLoader(this);
        this.props.getSeacrhEventsRequest(payload, () => {
          Util.hideLoader(this);
          this.setState({ isSearching: false });
        });
      }
_getSearch=(text)=>{
this.setState({isSearching:true});
// this._getSearchRequest(text);
}
    render() {
        const {data}=this.props
     const {isSearching}=this.state
        return (
            <View style={styles.navigationContainer}>

                <View style={styles.searchTopView}>
                    <Text style={styles.scheduleHeading}>Process A Card</Text>
                </View>

                <View style={styles.searchFieldContainer}>

                    <View style={styles.searchInput}>

                        <TextInput style={styles.input2} selectionColor='#900816' placeholder='Search Keyword' onChangeText={text =>this._getSearch(text) }></TextInput>
                    { 
                    isSearching && (
                    <ActivityIndicator
                    size="small"
                    color={"#fff"}
                     style={{marginRight:10}}
                      />)
                    }

                    </View>

                    <View style={{justifyContent:'center',alignItems:'center',padding:5}}>
                        <Image source={require('../../assets/images/filter.png')} style={styles.searchIconImage} />
                    </View>


                </View>

                <View style={styles.notificatioSearchnView}>



                    <FlatList

                        data={data}
                        renderItem={this.renderItems}
                        keyExtractor={item => item.key}
                        ItemSeparatorComponent={this.renderSeparator}
                    />



                </View>

            </View>
        )
    }
}
const mapStateToProps = ({cards}) => ({
    data:cards.data 
});

const actions = { };

export default connect(
  mapStateToProps,
  actions
)(searchComponent);