// /**
//  * Created by Waleed Arain on 05/09/2019.
//  */


import React, { Component } from "react";
import { Provider } from "react-redux";
import { AppRegistry, View, NativeModules,Alert } from "react-native";
import { MessageBar } from "./components";
import configureStore from "./store";
//import AppNavigator from "./navigator";
//import MainApp from "../src/src-app";
import Router from "./routes/router";

import applyConfigSettings from "./config";
import AppStyles from "./theme/AppStyles";
import Util from "./util";
import DataHandler from "./services/DataHandler";
import SplashScreen from "react-native-splash-screen";

const reducers = require("./reducers").default;

applyConfigSettings();

export default class Main extends Component {
  state = {
    isLoading: true,
    store: configureStore(reducers, () => {
      this._loadingCompleted();
      this.setState({ isLoading: false });
    })
  };

  _loadingCompleted() {
    DataHandler.setStore(this.state.store);
  }

  componentDidMount() {
    //Alert.alert("hi");
    SplashScreen.hide();

  }

  render() {
    if (this.state.isLoading) {
      return null;
    }

    return (
      <View style={AppStyles.flex}>
        <Provider store={this.state.store}>
          <Router />
        </Provider>
        <MessageBar />
      </View>
    );
  }
}

AppRegistry.registerComponent("WaterAdventure", () => App);
