// @flow

import { ABOUT_US,TERMS_AND_CONDITION,PRIVACY_POLICY } from "./ActionTypes";

export function aboutRequest(responseCallback) {
  return {
    responseCallback,
    type: ABOUT_US.REQUEST
  };
}
export function aboutSuccess(data,responseCallback) {
  return {
    data,responseCallback,

    type: ABOUT_US.SUCCESS
  };
}
export function termsRequest(responseCallback) {
  return {
    responseCallback,
    type: TERMS_AND_CONDITION.REQUEST
  };
}
export function termsSuccess(data,responseCallback) {
  return {
    data,responseCallback,

    type: TERMS_AND_CONDITION.SUCCESS
  };
}
export function privacyRequest(responseCallback) {
  return {
    responseCallback,
    type: PRIVACY_POLICY.REQUEST
  };
}
export function privacySuccess(data,responseCallback) {
  return {
    data,responseCallback,

    type: PRIVACY_POLICY.SUCCESS
  };
}

