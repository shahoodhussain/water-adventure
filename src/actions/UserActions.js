// @flow

import {
  USER_SIGNIN,
  USER_SIGNOUT,
  UPDATE_USER_PROFILE,
  USER_FORGOT_PASSWORD,
  USER_UPDATE_PASSWORD,

} from "./ActionTypes";


export function userSigninRequest(payload, responseCallback) {
  return {
    payload,
    responseCallback,
    type: USER_SIGNIN.REQUEST
  };
}

export function userSigninSuccess(data, access_token) {
  return {
    data,
    access_token,
    // save_token,
    type: USER_SIGNIN.SUCCESS
  };
}

export function userSignOutRequest(responseCallback) {
  return {
    responseCallback,
    type: USER_SIGNOUT.REQUEST
  };
}

export function userSignOutSuccess() {
  return {
    type: USER_SIGNOUT.SUCCESS
  };
}

export function updateUserProfileRequest(payload, responseCallback) {
  return {
    payload,
    responseCallback,
    type: UPDATE_USER_PROFILE.REQUEST
  };
}

export function updateUserProfileSuccess(data,responseCallback) {
  return {
    data,
    responseCallback,
    type: UPDATE_USER_PROFILE.SUCCESS
  };
}

export function forgotPasswordRequest(payload, responseCallback) {
  return {
    payload,
    responseCallback,
    type: USER_FORGOT_PASSWORD.REQUEST
  };
}



export function updatePasswordRequest(payload, responseCallback) {
  return {
    payload,
    responseCallback,
    type: USER_UPDATE_PASSWORD.REQUEST
  };
}
export function updatePasswordSuccess(token, responseCallback) {
  return {
    token,
    responseCallback,
    type: USER_UPDATE_PASSWORD.SUCCESS
  };
}
