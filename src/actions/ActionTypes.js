// @flow
const REQUEST = "REQUEST";
const SUCCESS = "SUCCESS";
const CANCEL = "CANCEL";
const FAILURE = "FAILURE";

function createRequestTypes(base) {
  const res = {};
  [REQUEST, SUCCESS, FAILURE, CANCEL].forEach(type => {
    res[type] = `${base}_${type}`;
  });
  return res;
}
// USER ACTIONS
export const NETWORK_INFO = "NETWORK_INFO";
export const USER_SIGNIN = createRequestTypes("USER_SIGNIN");
export const USER_SIGNOUT = createRequestTypes("USER_SIGNOUT");
export const UPDATE_USER_PROFILE = createRequestTypes("UPDATE_USER_PROFILE");
export const USER_FORGOT_PASSWORD = createRequestTypes("USER_FORGOT_PASSWORD");
export const USER_UPDATE_PASSWORD = createRequestTypes("USER_UPDATE_PASSWORD");
export const UPDATE_PROFILE = createRequestTypes("UPDATE_PROFILE");
export const LOGOUT = "LOGOUT";
export const EMPTY = createRequestTypes("EMPTY");
export const CLEAR_CHECKOUT_DATA=createRequestTypes("CLEAR_CHECKOUT_DATA");

// AppINFO Actions 
export const TERMS_AND_CONDITION = createRequestTypes("TERMS_AND_CONDITION");
export const ABOUT_US = createRequestTypes("ABOUT_US");
export const PRIVACY_POLICY = createRequestTypes("PRIVACY_POLICY");

// ACTIVITIES ACTIONS 
export const GET_ALL_ACTIVITIES=createRequestTypes("GET_ALL_ACTIVITIES");
export const CREATE_ACTIVITY=createRequestTypes("CREATE_ACTIVITY");
export const ACTIVITY_BOOKING_DATE=createRequestTypes("ACTIVITY_BOOKING_DATE");
export const GET_CLASS_TYPES=createRequestTypes("GET_CLASS_TYPES");
export const GET_ALL_PACKAGES=createRequestTypes("GET_ALL_PACKAGES");
export const ADD_ACTIVITY_PACKAGES=createRequestTypes("ADD_ACTIVITY_PACKAGES");
// card Actions
export const CREATE_CARD=createRequestTypes("CREATE_CARD");
export const GET_ALL_CARDS=createRequestTypes("GET_ALL_CARDS");
export const GET_SEARCH_CARDS = createRequestTypes("GET_SEARCH_CARDS");
export const GET_CART_ITEMS=createRequestTypes("GET_CART_ITEMS");
export const APPLY_VOUCHER_CODE=createRequestTypes("APPLY_VOUCHER_CODE");
export const CHECKOUT=createRequestTypes("CHECKOUT");
export const UPDATE_CART = createRequestTypes("UPDATE_CART");
export const REMOVE_CART = createRequestTypes("REMOVE_CART");
//NotificationActions
export const GET_ALL_NOTIFICATIONS=createRequestTypes("GET_ALL_NOTIFICATIONS");