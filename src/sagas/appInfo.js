import { take, put, call, fork } from "redux-saga/effects";
import {
TERMS_AND_CONDITION,
PRIVACY_POLICY,
ABOUT_US
} from "../actions/ActionTypes";
import { SAGA_ALERT_TIMEOUT } from "../constants";
import {
 termsSuccess,
aboutSuccess,
privacySuccess
} from "../actions/AppInfoActions";
import {
  PRIVACY_POLICY as PRIVACY_POLICY_URL,
  ABOUT_US as ABOUT_US_URL,
    TERMS_AND_CONDITION as TERMS_AND_CONDITION_URL,
  callRequest
} from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";
import DataHandler from "../services/DataHandler";

function alert(message, type = "error") {
  setTimeout(() => {
    Util.topAlert(message, type);
  }, SAGA_ALERT_TIMEOUT);
}

function* termsrequest() {
  while (true) {
    const { responseCallback } = yield take(TERMS_AND_CONDITION.REQUEST);
    try {
      const response = yield call(
        callRequest,
        TERMS_AND_CONDITION_URL,
        {},
        "",
        {},
        ApiSauce
      );
      if (response.success) {
     
        if (responseCallback) responseCallback(true, response.success.content);
        yield put(termsSuccess(response.success.content));
      } else {
        if (responseCallback) responseCallback(null, null);
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.message));
    }
  }
}
function* aboutusrequest() {
  while (true) {
    const { responseCallback } = yield take(ABOUT_US.REQUEST);
    try {
      const response = yield call(
        callRequest,
        ABOUT_US_URL,
        {},
        "",
        {},
        ApiSauce
      );
      if (response.success) {
     
        if (responseCallback) responseCallback(true, response.success.content);
        yield put(aboutSuccess(response.success.content));
      } else {
        if (responseCallback) responseCallback(null, null);
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.message));
    }
  }
}
function* privacypolicyRequest() {
  while (true) {
    const { responseCallback } = yield take(PRIVACY_POLICY.REQUEST);
    try {
      const response = yield call(
        callRequest,
        PRIVACY_POLICY_URL,
        {},
        "",
        {},
        ApiSauce
      );
      if (response.success) {
     
        if (responseCallback) responseCallback(true, response.success.content);
        yield put(privacySuccess(response.success.content));
      } else {
        if (responseCallback) responseCallback(null, null);
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.message));
    }
  }
}

export default function* root() {
 
  yield fork(termsrequest);
  yield fork(aboutusrequest);
  yield fork(privacypolicyRequest)

}
