import { fork } from "redux-saga/effects";
import user from "./user"; 
import appInfo from "./appInfo"
import activities from "./activities";
import notification from "./notification";
import cards from "./cards";

export default function* root() {
yield fork(user);
yield fork(appInfo);
yield fork(activities);
yield fork(notification);
yield fork(cards);
}
