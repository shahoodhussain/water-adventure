import { take, put, call, fork } from "redux-saga/effects";
import {
  USER_SIGNIN,
  USER_SIGNOUT,
  UPDATE_USER_PROFILE,
  USER_FORGOT_PASSWORD,
  USER_UPDATE_PASSWORD,
} from "../actions/ActionTypes";
import { SAGA_ALERT_TIMEOUT } from "../constants";
import {
  
  userSigninSuccess,
  userSignOutSuccess,
  updateUserProfileSuccess,
  updatePasswordSuccess
} from "../actions/UserActions";
import {
  
  USER_SIGNIN as USER_SIGNIN_URL,
  USER_SIGNOUT as USER_SIGNOUT_URL,
  UPDATE_USER_PROFILE as UPDATE_USER_PROFILE_URL,
  USER_FORGOT_PASSWORD as USER_FORGOT_PASSWORD_URL,
  USER_UPDATE_PASSWORD as USER_UPDATE_PASSWORD_URL,
  callRequest
} from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";

function alert(message, type = "error") {
  setTimeout(() => {
    Util.topAlert(message, type);
  }, SAGA_ALERT_TIMEOUT);
}

function* signin() {
  while (true) {
    const { payload, responseCallback } = yield take(USER_SIGNIN.REQUEST);
    try {
      const response = yield call(
        callRequest,
        USER_SIGNIN_URL,
        payload,
        "",
        {},
        ApiSauce
      );
      // console.log("response", response);
      if (response.status==1) {
        if (responseCallback) responseCallback(response.data, null);
        yield put(
          userSigninSuccess(response.data, response.access_token)
        );
      } else {
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      // console.log(err, "check");
      alert(Util.getErrorText(err.message));
    }
  }
}

function* signout() {
  while (true) {
    const { responseCallback } = yield take(USER_SIGNOUT.REQUEST);
    try {
      const response = yield call(
        callRequest,
        USER_SIGNOUT_URL,
        {},
        "",
        {},
        ApiSauce
      );
      if (response.message) {
        if (responseCallback) responseCallback(true, null);
        yield put(userSignOutSuccess());
      } else {
        alert("Something went wrong");
        yield put(userSignOutSuccess());
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.message));
      yield put(userSignOutSuccess());
    }
  }
}

function* updateUserProfile() {
  while (true) {
    const { payload, responseCallback } = yield take(
      UPDATE_USER_PROFILE.REQUEST
    );
    try {
      const response = yield call(
        callRequest,
        UPDATE_USER_PROFILE_URL,
        payload,
        "",
        {},
        ApiSauce
      );
      console.log(response,"update response");
      if (response.success) {
        if (responseCallback) responseCallback(true, null);
        yield put(updateUserProfileSuccess(response.success.data));
       //  alert("updated")
      } else {
        if (responseCallback) responseCallback(null, null);
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.error));
    }
  }
}

function* forgotPassword() {
  while (true) {
    const { payload, responseCallback } = yield take(
      USER_FORGOT_PASSWORD.REQUEST
    );
    try {
      const response = yield call(
        callRequest,
        USER_FORGOT_PASSWORD_URL,
        payload,
        "",
        {},
        ApiSauce
      );
      console.log(response,"fotgot response");
      if (response.message) {
        if (responseCallback) responseCallback(true, null);
        alert(response.message,type="success")
      } else {
        if (responseCallback) responseCallback(null, null);
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.message));
    }
  }
}


function* updatePassword() {
  while (true) {
    const { payload, responseCallback } = yield take(
      USER_UPDATE_PASSWORD.REQUEST
    );
    try {
      const response = yield call(
        callRequest,
        USER_UPDATE_PASSWORD_URL,
        payload,
        "",
        {},
        ApiSauce
      );
      console.log("passwordcheck", response);
      if (response.success) {
        if (responseCallback) responseCallback(true, null);
        yield put(updatePasswordSuccess(response.success.token,responseCallback))
      } else {
        if (responseCallback) responseCallback(null, null);
        alert("Something went wrong");
      }
    } catch (err) {
      if (responseCallback) responseCallback(null, err);
      alert(Util.getErrorText(err.error));
    }
  }
}








export default function* root() {
  yield fork(signout);
  yield fork(signin);
  yield fork(updateUserProfile);
  yield fork(forgotPassword);
  yield fork(updatePassword);
}
