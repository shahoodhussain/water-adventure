import { take, put, call, fork, takeLatest } from "redux-saga/effects";
import {
  GET_ALL_NOTIFICATIONS
} from "../actions/ActionTypes";
import { SAGA_ALERT_TIMEOUT } from "../constants";
import {
 getNotificationSuccess
} from "../actions/NotificationActions";
import {
    GET_ALL_NOTIFICATIONS as GET_ALL_NOTIFICATIONS_URL,
  callRequest
} from "../config/WebService";
import ApiSauce from "../services/ApiSauce";
import Util from "../util";

function alert(message, type = "error") {
    setTimeout(() => {
      Util.topAlert(message, type);
    }, SAGA_ALERT_TIMEOUT);
  }

function* getNotification() {
    while (true) {
      const { payload, responseCallback } = yield take(GET_ALL_NOTIFICATIONS.REQUEST);
      
      try {
        const response = yield call(
          callRequest,
          GET_ALL_NOTIFICATIONS_URL,
          {},
          "",
          {},
          ApiSauce
        );
        if (response.success) {
        
          if (responseCallback) responseCallback(true, null);
          yield put(getNotificationSuccess(response.success.data));
        } else {
         
          alert("Something went wrong");
        }
      } catch (err) {
        if (responseCallback) responseCallback(null, err);
        alert(Util.getErrorText(err.error));
      }
    }
  }
  export default function* root() {
    yield fork(getNotification);
   
  }