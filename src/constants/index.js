import moment from "moment";
import Util from "../util";
import { Images } from "../theme";

export const PASSWORD_MIN_LENGTH = 6;
export const PASSWORD_MAX_LENGTH = 20;
// export const TIME_ZONE = (-1 * new Date().getTimezoneOffset()) / 60;
export const APP_URL = "";
export const APP_DOMAIN = "";
export const QUERY_LIMIT = 10;
export const SAGA_ALERT_TIMEOUT = 500;
export const POST_VIEW_TIMEOUT = 2000;
export const IMAGE_QUALITY = 1;
export const IMAGE_MAX_WIDTH = 720;
export const IMAGE_MAX_HEIGHT = 480;
export const IMAGE_COMPRESS_MAX_WIDTH = 720;
export const IMAGE_COMPRESS_MAX_HEIGHT = 480;
export const VERSES_OF_THE_DAY_LIMIT = 10;
export const IMAGE_COMPRESS_FORMAT = "JPEG";
export const ANDROID_NOTI_CHANNEL = "VeteranAppChanel";

// date time formats
export const DATE_FORMAT1 = "dddd, DD MMMM, YYYY";
export const DATE_FORMAT2 = "DD MMM YYYY";
export const DATE_FORMAT3 = "YYYY-MM-DD";
export const TIME_FORMAT1 = "hh:mm A";
export const TIME_FORMAT2 = "HH:mm ";

export const DATE_FORMAT_TIME1 = "Do | MMM | HH";
export const DATE_FORMAT4 = "dddd, DD MMMM YYYY";
export const DATE_FORMAT5 = "MMM DD, YYYY";

// Messages

export const LOCATION_PERMISSION_DENIED_ERROR2 =
  "Location permission required, please go to app settings to allow access";
export const INVALID_NAME_ERROR = "Invalid name";
export const INVALID_EMAIL_ERROR = "Invalid email";
export const INVALID_PASSWORD_ERROR = `Invalid password, at least ${PASSWORD_MIN_LENGTH} characters`;
export const INTERNET_ERROR = "Please connect to the working internet";
export const ARE_U_SURE = "Are you sure?";
export const WELCOME_NOTE = "Welcome to the Veteran App!";
export const PROFILE_UPDATE_SUCCESS = "Profile successfully updated!";
export const SESSION_EXPIRED_ERROR = "Session expired, Please login again";

// Message types
export const MESSAGE_TYPES = {
  INFO: "info",
  ERROR: "error",
  SUCCESS: "success"
};

// File Types
export const FILE_TYPES = { VIDEO: "video", IMAGE: "image", AUDIO: "audi" };

// User Types
export const USER_TYPES = [
  {
    value: "Veteran",
    label: "Veteran"
  },
  {
    value: "Service Provider",
    label: "serviceProvider"
  }
];
export const weekdays = new Array(7);
weekdays[0] =  "Sun";
weekdays[1] = "Mon";
weekdays[2] = "Tue";
weekdays[3] = "Wed";
weekdays[4] = "Thu";
weekdays[5] = "Fri";
weekdays[6] = "Sat";

// Delta location

export const DELTA_LOCATION = {
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
};
export const notiList = [
  {
      name: 'lorem ispum is just a dummy text ',
      subtitle: '02:50 pm',
      key:"1"
  },
  {
      name: 'lorem ispum is just a dummy text ',
      subtitle: '04:50 am',
      key:"2"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '12:50 pm',
      key:"3"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '03:50 pm',
      key:"4"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"5"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"6"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"7"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"8"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"9"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"10"
  },
  {
      name: 'lorem ispum is just a dummy text',
      subtitle: '02:50 pm',
      key:"11"
  },
  {
      name: 'lorem ispum is just a dummy text  ',
      subtitle: '02:50 pm',
      key:"12"
  },
  {
      name: 'lorem ispum is just a dummy text  ',
      subtitle: '02:50 pm',
      key:"13"
  },

];


export const CLASS_LIST = [
  {
      name: 'Nathan Lews',
      subtitle: 'Broadway in MidTown New york United States',
      date:'5',
      day:'Mon',
      time:'15:00',
      key:"1",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'6',
      day:'Tue',
      time:'15:00',
      key:"2",
      route:'ClassDetail'
  },
  {
      name: 'Nathan Lews',
      subtitle: 'Broadway in MidTown New york United States',
      date:'7',
      day:'Wed',
      time:'13:00',
      key:"3",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Thur',
      time:'19:00',
      key:"4",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'9',
      day:'Fri',
      time:'21:00',
      key:"5",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Mon',
      time:'13:00',
      key:"6",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Mon',
      time:'12:00',
      key:"7",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Mon',
      time:'15:00',
      key:"8",
      route:'ClassDetail'
  },
  {
      name: 'Nathan Lews',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Mon',
      time:'15:00',
      key:"9",
      route:'ClassDetail'
  },
  {
      name: 'Mark Jones',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Mon',
      time:'15:00',
      key:"10",
      route:'ClassDetail'
  },
  {
      name: 'Nathan Lews',
      subtitle: 'Broadway in MidTown New york United States',
      date:'8',
      day:'Mon',
      time:'15:00',
      key:"11",
      route:'ClassDetail'
  },


];
export const cardList = [
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"1"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"2"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"3"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"4"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"5"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"6"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"7"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"8"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"9"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"10"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"11"
    },
    {
        name: 'Michael John ',
        subtitle: 'ID 29103849',
        key:"12"
    },
];