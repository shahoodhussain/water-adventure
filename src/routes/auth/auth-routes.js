/**
 * Created by waleed on 13/05/2019.
 */

import { createStackNavigator } from "react-navigation";
import { fromLeft } from 'react-navigation-transitions';
import {Image,View} from 'react-native';
import React, { Component } from "react";
import LoginComponent from "../../containers/login/login-component";
import ForgotPasswordComponent from "../../containers/forgot-password/forgot-password-component";
import WelcomeComponent from "../../containers/Welcome/index";

export default createStackNavigator(
  {
    Login: {
      screen: LoginComponent,
      navigationOptions: () => ({
        headerTitle:( 
        <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
        <Image
            resizeMode="center"
            source={require('../../assets/images/logo.png')}
            style={{width:110, height:50 }} //marginBottom: 20
        />
        </View> 
        ),
        })
    },
    ForgetPassword: {
      screen: ForgotPasswordComponent,
      navigationOptions: () => ({
        headerTitle:( 
          <View style={{flex:1, flexDirection:'row', justifyContent:'center'}}>
          <Image
              resizeMode="center"
              source={require('../../assets/images/logo.png')}
              style={{width:110, height:50}} //,marginBottom:20
          />
          </View> 
          ),
      })
    },
    Welcome: {
      screen: WelcomeComponent,
      navigationOptions: () => ({
        //headerTintColor: "#000",
      })
    },
    
  },
  {
    initialRouteName: "Login",
    mode: 'modal',
    transitionConfig: () => fromLeft(1000),
  }
);
