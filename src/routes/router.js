/**
 * Created by faraz ali on 13/05/2019.
 */

import {
  createSwitchNavigator,
  // createStackNavigator,
  createAppContainer
} from "react-navigation";
("Routes/app/app-routes");
import AppRoutes from "../routes/app/app-routes";
import AuthRoutes from "../routes/auth/auth-routes";

export default createAppContainer(
  createSwitchNavigator(
    {
      Auth: AuthRoutes,
      App: AppRoutes,
    },
    {
      initialRouteName: 'Auth'
    }
  )
);


