/**
 * Created by waleed on 13/05/2019.
 */

import {
  createStackNavigator
} from "react-navigation";

import HomePageComponent from "./../../containers/home/home-page-component";
import ActivityDetailPageComponent from "./../../containers/activity/settting-responsive";
import PackagesAccessoriesComponent from "../../containers/activity/packages-accessories-component";
import PackageInvoiceActivity from "../../containers/activity/activity-package-invoice";
import OrderDetails from "../../containers/activity/activity-order-details-component";

export default createStackNavigator(
  {
    
    Home: {
      screen: HomePageComponent,
      navigationOptions: () => ({
        headerBackTitle: null,
        headerTintColor:'#000'
      }),
    },

    ActivityDetail:{
      screen: ActivityDetailPageComponent,
      navigationOptions: () => ({
        headerBackTitle: null,
        headerTintColor:'#000'
      }),
    },
    PackagesAndAccessories:{
      screen: PackagesAccessoriesComponent,
      navigationOptions: () => ({
        headerBackTitle: null,
        headerTintColor:'#000'
      }),
    },
    ActivityInvoice:{
      screen: PackageInvoiceActivity,
      navigationOptions: () => ({
        headerBackTitle: null,
        headerTintColor:'#000'
      }),
    },
    OrderReceipt:{
      screen: OrderDetails,
      navigationOptions: () => ({
        headerBackTitle: null,
        headerTintColor:'#000',
        headerLeft: null
      }),
    }
  },
  {
    //headerBackTitleVisible: false,
    initialRouteName:'Home',
    mode: 'modal',
    //transitionConfig: () => fromLeft(),
  }
);
